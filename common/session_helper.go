package common

import (
	"fmt"

	"github.com/labstack/echo"
	"gitlab.com/gecthai/enginex/session"
	"gitlab.com/gecthai/enginex/util/stringutil"
	"gitlab.com/gecthai/webx/webserver_constant"
)

// TODO User-session
func GetViewModel(ctx echo.Context) map[string]interface{} {
	var vm map[string]interface{}
	if v, ok := ctx.Get(webserver_constant.ViewModelContextKey).(map[string]interface{}); ok {
		vm = v
	} else {
		vm = make(map[string]interface{})
	}

	return vm
}

func SessionReadStr(context echo.Context, sessionStoreName, sessionKey string) (string, error) {
	if context != nil && stringutil.IsNotEmptyString(sessionStoreName) && stringutil.IsNotEmptyString(sessionKey) {
		userSession := session.GET(sessionStoreName, context)
		if userSession != nil {
			iStr := userSession.Get(sessionKey)
			if value, ok := iStr.(string); ok {
				return value, nil
			}
		}
	}
	return "", fmt.Errorf("cannot read string from session store %s", sessionStoreName)
}
