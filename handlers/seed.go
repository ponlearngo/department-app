package handlers

import (
	"department-app/api"
	"department-app/bindings"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/gecthai/enginex/common_renderings"
	"gitlab.com/gecthai/webx/webserver_constant"
)

const (
	getPersonalHandlerName        = "GetPersonalHandler"
	getPersonalIndexSearchHandler = "GetPersonalIndexSearchHandler"
)

func SeedPageHandler(ctx echo.Context) error {
	//get default view model context
	var vm map[string]interface{}
	if v, ok := ctx.Get(webserver_constant.ViewModelContextKey).(map[string]interface{}); ok {
		vm = v
	} else {
		vm = make(map[string]interface{})
	}
	return ctx.Render(http.StatusOK, "seedPage", vm)
}

func UploadFilesApiHandler(ctx echo.Context) error {
	request := &bindings.UploadFilesRequest{}
	errResponse := &common_renderings.ErrorsResponse{}

	if err := ctx.Bind(request); err != nil {
		errResp := common_renderings.NewError("err0002", err.Error(), nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			viewCreditSupportApiHandlerName, errResponse)
	}

	form, err := ctx.MultipartForm()
	if err != nil {
		errResp := common_renderings.NewError("err0002", err.Error(), nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			viewCreditSupportApiHandlerName, errResponse)
	}

	respData, err := api.UploadFile(request, form)
	if err != nil {
		errResp := common_renderings.NewError("err0002", err.Error(), nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			viewCreditSupportApiHandlerName, errResponse)
	}

	resp := common_renderings.Response{
		Success:     true,
		MessageCode: "err0002",
		Message:     "upload success",
		Total:       0,
		Datas:       respData.Datas,
	}

	return ctx.JSON(http.StatusOK, resp)
}

// func SeedListHandler(c echo.Context) error {

// 	config, err := custom_configuration.Config()
// 	if err != nil {

// 	}

// 	//read context key from param
// 	dbContextKey := config.DataBaseContext

// 	dbConn, ok := webserver.Instant().DBConnections()[dbContextKey]
// 	if !ok {

// 	}

// 	if dbConn == nil {

// 	}
// 	results, err := datastore.GetPersonal(dbConn)
// 	if err != nil {

// 	}

// 	resp := common_renderings.Response{
// 		Success:     true,
// 		MessageCode: "psi0001",
// 		Message:     "Read data success",
// 		Datas:       results,
// 	}

// 	return ctx.JSON(http.StatusOK, resp)
// }
