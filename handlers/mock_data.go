package handlers

const (
	borrowerPageMackJson = `
	{
		"success": true,
		"message_code": "PSI00001",
		"message": "Read data success",
		"datas": {
			"borrower": {
				"first_name":"แก้วกานต์",
				"last_name":"วรรณลักษณ์",
				"images":["https://picsum.photos/500/200","https://picsum.photos/250/200","https://picsum.photos/200/200"]
			},
			"guarantor_1": {
			  "first_name":"ผู้ค้ำ1",
			  "last_name":"วรรณลักษณ์",
			  "images":["https://picsum.photos/450/200","https://picsum.photos/250/200","https://picsum.photos/200/200"]
			},
			"guarantor_2": {
			  "first_name":"ผู้ค้ำ2",
			  "last_name":"วรรณลักษณ์",
			  "images":["https://picsum.photos/350/200","https://picsum.photos/250/200","https://picsum.photos/200/200"]
			},
			"guarantor_3": {
			  "first_name":"ผู้ค้ำ3",
			  "last_name":"วรรณลักษณ์",
			  "images":["https://picsum.photos/250/250","https://picsum.photos/250/200","https://picsum.photos/200/200"]
			}
		}
	  }
	`

	addressPageMackJson = `
	{
		"success": true,
		"message_code": "PSI00001",
		"message": "Read data success",
		"datas": {
			"borrower": {
				"house_number":"123",
				"house_group":"5",
				"village_name":"วานารีเวลเลจ",
				"images":["https://picsum.photos/300/200","https://picsum.photos/250/200","https://picsum.photos/200/200"]
			},
			"guarantor_1": {
				"house_number":"123",
				"house_group":"5",
				"village_name":"วานารีเวลเลจ ผู้ค้ำ 1",
				"images":["https://picsum.photos/100/200","https://picsum.photos/250/200","https://picsum.photos/200/200"]
			},
			"guarantor_2": {
				"house_number":"123",
				"house_group":"5",
				"village_name":"วานารีเวลเลจ ผู้ค้ำ 2",
				"images":["https://picsum.photos/150/200","https://picsum.photos/250/200","https://picsum.photos/200/200"]
			},
			"guarantor_3": {
				"house_number":"123",
				"house_group":"5",
				"village_name":"วานารีเวลเลจ ผู้ค้ำ 3",
				"images":["https://picsum.photos/230/200","https://picsum.photos/250/200","https://picsum.photos/200/200"]
			}
		  }
	  }
	`

	creditSupportMockJson = `
	{
		"success": true,
		"message_code": "PSI00001",
		"message": "Read data success",
		"datas": {
			"tab_borrower": {
			  "borrower": {
				"first_name":"แก้วกานต์",
				"last_name":"วรรณลักษณ์",
				"images":["https://picsum.photos/500/200","https://picsum.photos/250/200","https://picsum.photos/200/200"]
			  },
			  "guarantor_1": {
				"first_name":"ผู้ค้ำ1",
				"last_name":"วรรณลักษณ์",
				"images":["https://picsum.photos/450/200","https://picsum.photos/250/200","https://picsum.photos/200/200"]
			  },
			  "guarantor_2": {
				"first_name":"ผู้ค้ำ2",
				"last_name":"วรรณลักษณ์",
				"images":["https://picsum.photos/350/200","https://picsum.photos/250/200","https://picsum.photos/200/200"]
			  },
			  "guarantor_3": {
				"first_name":"ผู้ค้ำ3",
				"last_name":"วรรณลักษณ์",
				"images":["https://picsum.photos/250/250","https://picsum.photos/250/200","https://picsum.photos/200/200"]
			  }
			},
			"tab_address": {
			  "borrower": {
				"house_number":"123",
				"house_group":"5",
				"village_name":"วานารีเวลเลจ",
				"images":["https://picsum.photos/300/200","https://picsum.photos/250/200","https://picsum.photos/200/200"]
			  },
			  "guarantor_1": {
				"house_number":"123",
				"house_group":"5",
				"village_name":"วานารีเวลเลจ ผู้ค้ำ 1",
				"images":["https://picsum.photos/100/200","https://picsum.photos/250/200","https://picsum.photos/200/200"]
			  },
			  "guarantor_2": {
				"house_number":"123",
				"house_group":"5",
				"village_name":"วานารีเวลเลจ ผู้ค้ำ 2",
				"images":["https://picsum.photos/150/200","https://picsum.photos/250/200","https://picsum.photos/200/200"]
			  },
			  "guarantor_3": {
				"house_number":"123",
				"house_group":"5",
				"village_name":"วานารีเวลเลจ ผู้ค้ำ 3",
				"images":["https://picsum.photos/230/200","https://picsum.photos/250/200","https://picsum.photos/200/200"]
			  }
			}
		  }
	  }
	`
)
