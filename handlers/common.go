package handlers

import (
	"github.com/labstack/echo"
	"gitlab.com/gecthai/enginex/common_renderings"
	log "gitlab.com/gecthai/enginex/echo_logrus"
	"gitlab.com/gecthai/enginex/util/stringutil"
)

const (
	// TODO User-session
	returnToLoginPageHTML = `<html><body onload="window.location.href='./login.html';"></body></html>`
)

type ResponseData struct {
	Success     bool        `json:"success"`
	MessageCode string      `json:"message_code"`
	Message     string      `json:"message"`
	Total       int64       `json:"total"`
	Datas       interface{} `json:"datas"`
}

func ThrowErrorResponse(ctx echo.Context,
	httpStatus int,
	messageCode,
	message string,
	forwardUrl string,
	functionName string,
	errResponse *common_renderings.ErrorsResponse) error {

	response := &common_renderings.Response{
		StatusCode:     httpStatus,
		Success:        false,
		MessageCode:    messageCode,
		Message:        message,
		Total:          0,
		ForwardURL:     forwardUrl,
		Datas:          nil,
		ErrorsResponse: *errResponse,
	}

	if errResponse != nil && errResponse.Len() > 0 {
		response.Message = errResponse.Error()
	}

	if stringutil.IsNotEmptyString(functionName) {
		log.Errorf("%s --> %s", functionName, response.Message)
	} else {
		log.Errorf("%s", response.Message)
	}

	response.BindBody()

	return ctx.JSON(httpStatus, response)
}
