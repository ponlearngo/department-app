package render

import (
	"encoding/json"

	"gitlab.com/gecthai/enginex/util/stringutil"
)

type DepartmentResponse struct {
	DepartmentId   string `json:"department_id"`
	DepartmentName string `json:"department_name"`
	Address        string `json:"address"`
	Email          string `json:"email"`
}

func (resp DepartmentResponse) String() string {
	return stringutil.Json(resp)
}

type DepartmentIndexResponse struct {
	SearchIndex string `json:"search_index"`
}

func (pri DepartmentIndexResponse) String() string {
	jsonRaw, err := json.MarshalIndent(&pri, " ", " ")
	if err != nil {
		return ""
	}
	return string(jsonRaw)
}
