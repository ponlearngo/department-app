package handlers

import (
	"department-app/bindings"
	"department-app/custom_configuration"
	"department-app/datastore"
	"department-app/handlers/message"
	"fmt"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/gecthai/enginex/common_renderings"
	"gitlab.com/gecthai/webx/webserver"
	"gitlab.com/gecthai/webx/webserver_constant"
)

const (
	// getPersonalHandlerName        = "GetPersonalHandler"
	// getPersonalIndexSearchHandler = "GetPersonalIndexSearchHandler"
	// getMasterCreditScoringSearchHandlerV1Name = "GetMasterCreditScoringSearchHandlerV1"
	getDepartmentListHandlerName   = "GetDepartmentListHandlerName"
	viewAddDepartmentApiHandler    = "AddPersonApiHandler"
	viewSearchDepartmentApiHandler = "SearchDepartmentApiHandler"
	viewDeleteDepartmentApiHandle  = "DeleteDepartmentApiHandler"
)

func DepartmentDetailPageHandler(ctx echo.Context) error {
	//get default view model context
	var vm map[string]interface{}
	if v, ok := ctx.Get(webserver_constant.ViewModelContextKey).(map[string]interface{}); ok {
		vm = v
	} else {
		vm = make(map[string]interface{})
	}
	return ctx.Render(http.StatusOK, "departmentDetailPage", vm)
}

func DepartmentPageHandler(ctx echo.Context) error {
	//get default view model context
	var vm map[string]interface{}
	if v, ok := ctx.Get(webserver_constant.ViewModelContextKey).(map[string]interface{}); ok {
		vm = v
	} else {
		vm = make(map[string]interface{})
	}
	return ctx.Render(http.StatusOK, "departmentPage", vm)
}

func DepartmentApiHandler(ctx echo.Context) error {
	conf, err := custom_configuration.Config()
	if err != nil {
		return message.ThrowErrorResponse(ctx,
			http.StatusBadRequest, message.StatusBadRequestCode, "err002", err.Error(),
			getDepartmentListHandlerName, (&common_renderings.ErrorsResponse{}).
				Append(message.StatusBadRequestError(err)))
	}

	dbContextKey := conf.DataBaseContext

	dbConn, ok := webserver.Instant().DBConnections()[dbContextKey]
	if !ok {
		errResp := common_renderings.NewError("pse0002",
			fmt.Sprintf("database connection in context %s not found", dbContextKey),
			nil)
		return message.ThrowErrorResponse(ctx,
			http.StatusBadRequest, message.StatusBadRequestCode, "err002", "",
			getDepartmentListHandlerName, (&common_renderings.ErrorsResponse{}).
				Append(message.StatusBadRequestError(errResp)))
	}

	results, totals, err := datastore.GetDepartmentList(dbConn)
	if err != nil {
		return message.ThrowErrorResponse(ctx,
			http.StatusBadRequest, message.StatusBadRequestCode, "err002", err.Error(),
			getDepartmentListHandlerName, (&common_renderings.ErrorsResponse{}).
				Append(message.StatusBadRequestError(err)))
	}

	resp := &ResponseData{
		Success:     true,
		MessageCode: "PSI00001",
		Message:     "Read data success",
		Total:       totals,
		Datas:       results,
	}

	return ctx.JSON(http.StatusOK, resp)
}

func AddPersonApiHandler(ctx echo.Context) error {
	request := &bindings.DepartmentAddRequest{}
	errResponse := &common_renderings.ErrorsResponse{}

	if err := ctx.Bind(request); err != nil {
		errResp := common_renderings.NewError("err0002", err.Error(), nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			viewAddDepartmentApiHandler, errResponse)
	}

	if err := ctx.Validate(request); err != nil {
		if validateErrResponse, ok := err.(common_renderings.ErrorsResponse); ok {
			return message.ThrowErrorResponse(ctx,
				http.StatusBadRequest, message.StatusBadRequestCode, "", "",
				viewAddDepartmentApiHandler, &validateErrResponse)
		} else {
			return message.ThrowErrorResponse(ctx,
				http.StatusBadRequest, message.StatusBadRequestCode, "", "",
				viewAddDepartmentApiHandler, (&common_renderings.ErrorsResponse{}).
					Append(message.StatusBadRequestError(err)))
		}
	}

	conf, err := custom_configuration.Config()
	if err != nil {
		return message.ThrowErrorResponse(ctx,
			http.StatusBadRequest, message.StatusBadRequestCode, "err002", err.Error(),
			getDepartmentListHandlerName, (&common_renderings.ErrorsResponse{}).
				Append(message.StatusBadRequestError(err)))
	}

	dbContextKey := conf.DataBaseContext

	dbConn, ok := webserver.Instant().DBConnections()[dbContextKey]
	if !ok {
		errResp := common_renderings.NewError("pse0002",
			fmt.Sprintf("database connection in context %s not found", dbContextKey),
			nil)
		return message.ThrowErrorResponse(ctx,
			http.StatusBadRequest, message.StatusBadRequestCode, "err002", "",
			getDepartmentListHandlerName, (&common_renderings.ErrorsResponse{}).
				Append(message.StatusBadRequestError(errResp)))
	}

	deparment := &datastore.Department{
		DepartmentName: request.DepartmentName,
		Address:        request.Address,
		Email:          request.Email,
	}
	dep, _, err := datastore.InstertDepartment(dbConn, deparment)

	return ctx.JSON(http.StatusOK, dep)
}

func SearchDepartmentApiHandler(ctx echo.Context) error {
	request := &bindings.DepartmentSearchRequest{}
	errResponse := &common_renderings.ErrorsResponse{}

	if err := ctx.Bind(request); err != nil {
		errResp := common_renderings.NewError("err0002", err.Error(), nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			viewSearchDepartmentApiHandler, errResponse)
	}

	conf, err := custom_configuration.Config()
	if err != nil {
		return message.ThrowErrorResponse(ctx,
			http.StatusBadRequest, message.StatusBadRequestCode, "err002", err.Error(),
			getDepartmentListHandlerName, (&common_renderings.ErrorsResponse{}).
				Append(message.StatusBadRequestError(err)))
	}

	dbContextKey := conf.DataBaseContext

	dbConn, ok := webserver.Instant().DBConnections()[dbContextKey]
	if !ok {
		errResp := common_renderings.NewError("pse0002",
			fmt.Sprintf("database connection in context %s not found", dbContextKey),
			nil)
		return message.ThrowErrorResponse(ctx,
			http.StatusBadRequest, message.StatusBadRequestCode, "err002", "",
			getDepartmentListHandlerName, (&common_renderings.ErrorsResponse{}).
				Append(message.StatusBadRequestError(errResp)))
	}

	filteredData, totals, err := datastore.GetDepartmentBuilder(dbConn, request)

	resp := &ResponseData{
		Success:     true,
		MessageCode: "PSI00001",
		Message:     "Read data success",
		Total:       totals,
		Datas:       filteredData,
	}

	return ctx.JSON(http.StatusOK, resp)
}

func DeleteDepartmentApiHandler(ctx echo.Context) error {
	request := &bindings.DepartmentDeleteRequest{}
	errResponse := &common_renderings.ErrorsResponse{}

	if err := ctx.Bind(request); err != nil {
		errResp := common_renderings.NewError("err0002", err.Error(), nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			viewDeleteDepartmentApiHandle, errResponse)
	}

	conf, err := custom_configuration.Config()
	if err != nil {
		return message.ThrowErrorResponse(ctx,
			http.StatusBadRequest, message.StatusBadRequestCode, "err002", err.Error(),
			getDepartmentListHandlerName, (&common_renderings.ErrorsResponse{}).
				Append(message.StatusBadRequestError(err)))
	}

	dbContextKey := conf.DataBaseContext

	dbConn, ok := webserver.Instant().DBConnections()[dbContextKey]
	if !ok {
		errResp := common_renderings.NewError("pse0002",
			fmt.Sprintf("database connection in context %s not found", dbContextKey),
			nil)
		return message.ThrowErrorResponse(ctx,
			http.StatusBadRequest, message.StatusBadRequestCode, "err002", "",
			getDepartmentListHandlerName, (&common_renderings.ErrorsResponse{}).
				Append(message.StatusBadRequestError(errResp)))
	}
	datastore.DeleteDepartment(dbConn, request)

	resp := &ResponseData{
		Success:     true,
		MessageCode: "PSI00001",
		Message:     "Read data success",
		Total:       0,
		Datas:       nil,
	}

	return ctx.JSON(http.StatusOK, resp)
}
