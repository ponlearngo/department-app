package handlers

import (
	"department-app/bindings"
	"department-app/custom_configuration"
	"department-app/custom_constant"
	"encoding/json"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/gecthai/enginex/common_renderings"
	log "gitlab.com/gecthai/enginex/echo_logrus"
	"gitlab.com/gecthai/enginex/session"
	"gitlab.com/gecthai/webx/webserver_constant"
)

const (
	viewCreditSupportApiHandlerName = "ViewCreditSupportApiHandler"
)

func BorrowerPageHandler(ctx echo.Context) error {
	//get default view model context
	var vm map[string]interface{}
	if v, ok := ctx.Get(webserver_constant.ViewModelContextKey).(map[string]interface{}); ok {
		vm = v
	} else {
		vm = make(map[string]interface{})
	}

	conf, err := custom_configuration.Config()
	if err != nil {
		log.Errorf("masterCreditScoringEditPage : %s", err.Error())
		return ctx.HTML(http.StatusInternalServerError, returnToLoginPageHTML)
	}

	code, err := SessionReadStr(ctx, conf.SessionStoreName, custom_constant.SessionViewCreditSupportKey)
	if err != nil {
		log.Errorf("masterCreditScoringEditPage : %s", err.Error())
		return ctx.HTML(http.StatusInternalServerError, returnToLoginPageHTML)
	}

	// CALL SERVICE GET BOWWERDATA AND SET TO vm
	log.Debugf("%s", code)
	Respdata := common_renderings.Response{}
	err = json.Unmarshal([]byte(borrowerPageMackJson), &Respdata)
	if err != nil {
		ctx.HTML(http.StatusInternalServerError, returnToLoginPageHTML)
	}
	vm["borrowerData"] = Respdata
	//
	vm["page"] = "borrower"
	return ctx.Render(http.StatusOK, "borrowerPage", vm)
}

func AddressPageHandler(ctx echo.Context) error {
	//get default view model context
	var vm map[string]interface{}
	if v, ok := ctx.Get(webserver_constant.ViewModelContextKey).(map[string]interface{}); ok {
		vm = v
	} else {
		vm = make(map[string]interface{})
	}

	conf, err := custom_configuration.Config()
	if err != nil {
		log.Errorf("masterCreditScoringEditPage : %s", err.Error())
		return ctx.HTML(http.StatusInternalServerError, returnToLoginPageHTML)
	}

	code, err := SessionReadStr(ctx, conf.SessionStoreName, custom_constant.SessionViewCreditSupportKey)
	if err != nil {
		log.Errorf("masterCreditScoringEditPage : %s", err.Error())
		return ctx.HTML(http.StatusInternalServerError, returnToLoginPageHTML)
	}

	// CALL SERVICE GET BOWWERDATA AND SET TO vm
	log.Debugf("%s", code)
	Respdata := common_renderings.Response{}
	err = json.Unmarshal([]byte(addressPageMackJson), &Respdata)
	if err != nil {
		ctx.HTML(http.StatusInternalServerError, returnToLoginPageHTML)
	}
	vm["addressData"] = Respdata
	//
	vm["page"] = "address"
	return ctx.Render(http.StatusOK, "addressPage", vm)
}

func CreditSupportViewPageHandler(ctx echo.Context) error {
	//get default view model context
	var vm map[string]interface{}
	if v, ok := ctx.Get(webserver_constant.ViewModelContextKey).(map[string]interface{}); ok {
		vm = v
	} else {
		vm = make(map[string]interface{})
	}

	conf, err := custom_configuration.Config()
	if err != nil {
		log.Errorf("masterCreditScoringEditPage : %s", err.Error())
		return ctx.HTML(http.StatusInternalServerError, returnToLoginPageHTML)
	}

	code, err := SessionReadStr(ctx, conf.SessionStoreName, custom_constant.SessionViewCreditSupportKey)
	if err != nil {
		log.Errorf("masterCreditScoringEditPage : %s", err.Error())
		return ctx.HTML(http.StatusInternalServerError, returnToLoginPageHTML)
	}

	// CALL SERVICE GET BOWWERDATA AND SET TO vm
	log.Debugf("%s", code)
	Respdata := common_renderings.Response{}
	err = json.Unmarshal([]byte(creditSupportMockJson), &Respdata)
	if err != nil {
		ctx.HTML(http.StatusInternalServerError, returnToLoginPageHTML)
	}
	vm["creditSupportData"] = Respdata
	//

	return ctx.Render(http.StatusOK, "creditSupportPage", vm)
}

func ViewCreditSupportApiHandler(ctx echo.Context) error {
	request := &bindings.ViewCreditSupportRequest{}
	errResponse := &common_renderings.ErrorsResponse{}

	if err := ctx.Bind(request); err != nil {
		errResp := common_renderings.NewError("err0002", err.Error(), nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			viewCreditSupportApiHandlerName, errResponse)
	}

	conf, err := custom_configuration.Config()
	if err != nil {
		errResp := common_renderings.NewError("err0002", err.Error(), nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			viewCreditSupportApiHandlerName, errResponse)
	}

	userSession := session.GET(conf.SessionStoreName, ctx)
	if userSession == nil {
		errResp := common_renderings.NewError("pse0002", "user session not found", nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			viewCreditSupportApiHandlerName, errResponse)
	}

	userSession.Set(custom_constant.SessionViewCreditSupportKey, request.CreditCode)

	err = userSession.Save()
	if err != nil {
		errResp := common_renderings.NewError("err0002", err.Error(), nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			viewCreditSupportApiHandlerName, errResponse)
	}

	forwardUrl := ""
	if request.Type == "multiple" {
		forwardUrl = "./credit-support-borrower.html"
	}
	if request.Type == "single" {
		forwardUrl = "./credit-support-view.html"
	}

	resp := common_renderings.Response{
		Success:     true,
		MessageCode: "err0002",
		Message:     "request for view success",
		ForwardURL:  forwardUrl, //URL ของหน้า view
		Total:       0,
		Datas:       nil,
	}

	return ctx.JSON(http.StatusOK, resp)
}
