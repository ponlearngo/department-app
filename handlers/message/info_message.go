package message

import "fmt"

const (
	GI00001  = "gi00001"
	KSI00002 = "ks00002"
)

var CommonInformationMessages = map[string]CommonInformationMessage{
	GI00001: {
		MessageCode: GI00001,
		Message:     func(params ...string) string { return fmt.Sprintf("greeting [%s] success", params[0]) },
	},
	KSI00002: {
		MessageCode: KSI00002,
		Message: func(params ...string) string {
			return fmt.Sprintf("create MasterCreditScoring %s %s success", params[0], params[1])
		},
	},
}
