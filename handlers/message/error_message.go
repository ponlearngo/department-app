package message

import (
	"gitlab.com/gecthai/enginex/common_renderings"
)

var (
	KSE00001 = common_renderings.NewError("kse00001",
		"Department name is require", nil)
	KSE00002 = common_renderings.NewError("kse00002",
		"Email is require", nil)
	KSE00003 = common_renderings.NewError("kse00003",
		"Email invalid format", nil)
	KSE00004 = common_renderings.NewError("kse00004",
		"Address is require", nil)
)
