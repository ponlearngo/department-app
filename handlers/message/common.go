package message

import (
	"github.com/labstack/echo"
	"gitlab.com/gecthai/enginex/common_renderings"
	log "gitlab.com/gecthai/enginex/echo_logrus"
	"gitlab.com/gecthai/enginex/util/stringutil"
)

func ThrowErrorResponse(ctx echo.Context,
	httpStatus int,
	messageCode,
	message string,
	forwardUrl string,
	functionName string,
	errResponse *common_renderings.ErrorsResponse) error {

	var response *common_renderings.Response
	if errResponse == nil {
		response = &common_renderings.Response{
			Success:     false,
			MessageCode: messageCode,
			Message:     message,
			Total:       0,
			ForwardURL:  forwardUrl,
			Datas:       nil,
		}
	} else {
		response = &common_renderings.Response{
			Success:     false,
			MessageCode: messageCode,
			Message:     message,
			Total:       0,
			ForwardURL:  forwardUrl,
			Datas:       *errResponse,
		}
	}

	if errResponse != nil && errResponse.Len() > 0 {
		response.Message = errResponse.Error()
	}

	if stringutil.IsNotEmptyString(functionName) {
		log.Errorf("%s --> %s", functionName, response.Message)
	} else {
		log.Errorf("%s", response.Message)
	}

	return ctx.JSON(httpStatus, response)
}

const (
	StatusBadRequestCode          = "e00001"
	StatusInternalServerErrorCode = "e00002"
	StatusNotFoundCode            = "e00003"
	StatusForbiddenCode           = "e00004"
	StatusUnauthorizedCode        = "e00005"
	StatusFoundCode               = "e00006"
)

var (
	StatusBadRequestError = func(err error) *common_renderings.Error {
		return DefaultError("e00001", err)
	}

	StatusInternalServerError = func(err error) *common_renderings.Error {
		return DefaultError("e00002", err)
	}

	StatusNotFoundError = func(err error) *common_renderings.Error {
		return DefaultError("e00003", err)
	}

	StatusForbiddenError = func(err error) *common_renderings.Error {
		return DefaultError("e00004", err)
	}

	StatusUnauthorizedError = func(err error) *common_renderings.Error {
		return DefaultError("e00005", err)
	}

	StatusFoundError = func(err error) *common_renderings.Error {
		return DefaultError("e00006", err)
	}
	DefaultError = func(errCode string, err error) *common_renderings.Error {
		return common_renderings.NewError(errCode, err.Error(), nil)
	}
)

//error and info message for handler
type CommonInformationMessage struct {
	MessageCode string                 `json:"message_code"`
	Message     func(...string) string `json:"message"`
}

func (msg CommonInformationMessage) String() string {
	return stringutil.JsonPretty(msg)
}
