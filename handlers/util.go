package handlers

import (
	"department-app/custom_configuration"
	"errors"
	"fmt"

	"github.com/labstack/echo"
	"gitlab.com/gecthai/enginex/redisstore"
	"gitlab.com/gecthai/enginex/session"
	"gitlab.com/gecthai/enginex/util/stringutil"
	"gitlab.com/gecthai/webx/webserver"
)

// TODO User-session
func getCacheConnection() (*redisstore.RedisCacheStore, error) {
	cacheConnections := webserver.Instant().CacheStoreConnections()
	if cacheConnections == nil {
		return nil, errors.New("not found cache connection")
	}

	customConf, err := custom_configuration.Config()
	if err != nil {
		return nil, err
	}

	cacheConn := cacheConnections[customConf.CacheStoreName]
	if cacheConn == nil {
		return nil, errors.New("not found redis cache store")
	}

	return cacheConn, nil
}

func SessionReadStr(context echo.Context, sessionStoreName, sessionKey string) (string, error) {
	if context != nil && stringutil.IsNotEmptyString(sessionStoreName) && stringutil.IsNotEmptyString(sessionKey) {
		userSession := session.GET(sessionStoreName, context)
		if userSession != nil {
			iStr := userSession.Get(sessionKey)
			if value, ok := iStr.(string); ok {
				return value, nil
			}
		}
	}
	return "", fmt.Errorf("cannot read string from session store %s", sessionStoreName)
}
