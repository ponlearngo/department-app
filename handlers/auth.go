package handlers

import (
	"department-app/api"
	"department-app/bindings"
	"department-app/custom_configuration"
	"department-app/custom_constant"
	"department-app/renderings"
	"net/http"

	"github.com/labstack/echo"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/gecthai/enginex/common_renderings"
	"gitlab.com/gecthai/enginex/session"
	"gitlab.com/gecthai/webx/webserver_constant"
)

const (
	loginApiHandlerName   = "LoginApiHandler"
	signoutApiHandlerName = "SignoutApiHandler"
)

func LoginPageHandler(ctx echo.Context) error {
	//get default view model context
	var vm map[string]interface{}
	if v, ok := ctx.Get(webserver_constant.ViewModelContextKey).(map[string]interface{}); ok {
		vm = v
	} else {
		vm = make(map[string]interface{})
	}

	return ctx.Render(http.StatusOK, "loginPage", vm)
}

func LoginApiHandler(ctx echo.Context) error {
	request := &bindings.LoginRequest{}
	errResponse := &common_renderings.ErrorsResponse{}

	if err := ctx.Bind(request); err != nil {
		errResp := common_renderings.NewError("err0002", err.Error(), nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			loginApiHandlerName, errResponse)
	}

	if err := ctx.Validate(request); err != nil {
		if validateErrResponse, ok := err.(common_renderings.ErrorsResponse); ok {
			return ThrowErrorResponse(ctx,
				http.StatusBadRequest, "err0002", "", "",
				loginApiHandlerName, &validateErrResponse)
		} else {
			errResp := common_renderings.NewError("err0002", err.Error(), nil)
			errResponse.Append(errResp)
			return ThrowErrorResponse(ctx,
				http.StatusBadRequest, "err0002", "", "",
				loginApiHandlerName, errResponse)
		}
	}

	customConf, err := custom_configuration.Config()
	if err != nil {
		errResp := common_renderings.NewError("err0002", err.Error(), nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			loginApiHandlerName, errResponse)
	}

	apiResponse, err := api.Login(request)
	if err != nil {
		errResp := common_renderings.NewError("err0002", err.Error(), nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			loginApiHandlerName, errResponse)
	}

	userAccountRaw, match := apiResponse.Datas.(map[string]interface{})
	if !match {
		errResp := common_renderings.NewError("err0002", err.Error(), nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			loginApiHandlerName, errResponse)
	}

	userAccount := &renderings.UserData{}
	decodeCfg := &mapstructure.DecoderConfig{
		Metadata: nil,
		Result:   userAccount,
		TagName:  "json",
	}
	decoder, err := mapstructure.NewDecoder(decodeCfg)
	if err != nil {
		errResp := common_renderings.NewError("err0002", err.Error(), nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			loginApiHandlerName, errResponse)
	}
	err = decoder.Decode(userAccountRaw)
	if err != nil {
		errResp := common_renderings.NewError("err0002", err.Error(), nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			loginApiHandlerName, errResponse)
	}

	userSession := session.GET(customConf.SessionStoreName, ctx)
	if userSession == nil {
		errResp := common_renderings.NewError("err0002", err.Error(), nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			loginApiHandlerName, errResponse)
	}

	userSession.Set(custom_constant.UserSessionKey, userAccount.UserId)
	err = userSession.Save()
	if err != nil {
		errResp := common_renderings.NewError("err0002", err.Error(), nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			loginApiHandlerName, errResponse)
	}

	resp := common_renderings.Response{
		Success:     true,
		MessageCode: "err0002",
		Message:     "login success",
		ForwardURL:  "./product-list.html",
		Total:       0,
		Datas:       nil,
	}

	return ctx.JSON(http.StatusOK, resp)
}

func SignoutApiHandler(ctx echo.Context) error {
	errResponse := &common_renderings.ErrorsResponse{}

	customConf, err := custom_configuration.Config()
	if err != nil {
		errResp := common_renderings.NewError("err0002", err.Error(), nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			signoutApiHandlerName, errResponse)
	}

	userSession := session.GET(customConf.SessionStoreName, ctx)
	if userSession == nil {
		errResp := common_renderings.NewError("sgo00003", "session store not found", nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			signoutApiHandlerName, errResponse)
	}

	userId, err := SessionReadStr(ctx, customConf.SessionStoreName, custom_constant.UserSessionKey)
	if err != nil {
		errResp := common_renderings.NewError("err0002", err.Error(), nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			signoutApiHandlerName, errResponse)
	}

	err = api.Signout(userId)
	if err != nil {
		errResp := common_renderings.NewError("err0002", err.Error(), nil)
		errResponse.Append(errResp)
		return ThrowErrorResponse(ctx,
			http.StatusBadRequest, "err0002", "", "",
			signoutApiHandlerName, errResponse)
	}

	userSession.Clear()
	userSession.Save()

	resp := common_renderings.Response{
		Success:     true,
		MessageCode: "err0002",
		Message:     "signout success",
		ForwardURL:  customConf.LoginPageURL,
		Total:       0,
		Datas:       nil,
	}

	return ctx.JSON(http.StatusOK, resp)
}
