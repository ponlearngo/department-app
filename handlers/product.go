package handlers

import (
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/gecthai/webx/webserver_constant"
)

func ProductPageHandler(ctx echo.Context) error {
	//get default view model context
	var vm map[string]interface{}
	if v, ok := ctx.Get(webserver_constant.ViewModelContextKey).(map[string]interface{}); ok {
		vm = v
	} else {
		vm = make(map[string]interface{})
	}

	// TODO User-session
	// userSessionData, err := checkSession(ctx)
	// if err != nil {
	// 	ctx.HTML(http.StatusInternalServerError, returnToLoginPageHTML)
	// }
	// vm["userData"] = userSessionData
	// END
	return ctx.Render(http.StatusOK, "productPage", vm)
}
