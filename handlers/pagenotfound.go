package handlers

import (
	"github.com/labstack/echo"
	"net/http"
)

func PageNotFoundHandler(c echo.Context) error {
	return c.String(http.StatusNotFound,"page not found")
}