package api

import (
	"department-app/bindings"
	"department-app/custom_configuration"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-resty/resty/v2"
	"gitlab.com/gecthai/enginex/common_renderings"
)

const (
	LoginEndpoint   = "login"
	SignoutEndpoint = "signout"
)

func getIdentityServiceUrl() (string, error) {
	cfg, err := custom_configuration.Config()
	if err != nil {
		return "", err
	}

	return cfg.IdentityServiceUrl, nil
}

func Login(req *bindings.LoginRequest) (*common_renderings.Response, error) {
	getUrl, err := getIdentityServiceUrl()
	if err != nil {
		return nil, err
	}

	endpoint := fmt.Sprintf("%s%s", getUrl, LoginEndpoint)

	dataResp := &common_renderings.Response{}

	// Create a Resty Client
	client := resty.New()

	// POST JSON string
	// No need to set content type, if you have client level setting
	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(req).
		Post(endpoint)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("api error %d: %s", resp.StatusCode(), string(resp.Body()))
	}

	// TODO: remove when implement , POSTMAN did not response HEADER application/json.
	err = json.Unmarshal(resp.Body(), dataResp)
	if err != nil {
		return nil, err
	}

	return dataResp, nil
}

func Signout(userId string) error {
	getUrl, err := getIdentityServiceUrl()
	if err != nil {
		return err
	}

	endpoint := fmt.Sprintf("%s%s", getUrl, SignoutEndpoint)

	dataResp := &common_renderings.Response{}

	// Create a Resty Client
	client := resty.New()

	// POST JSON string
	// No need to set content type, if you have client level setting
	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(map[string]interface{}{
			"user_id": userId,
		}).
		//set model json
		//SetResult(personalResp).
		Post(endpoint)
	if err != nil {
		return err
	}

	if resp.StatusCode() != http.StatusOK {
		return fmt.Errorf("api error %d: %s", resp.StatusCode(), string(resp.Body()))
	}

	// TODO: remove when implement , POSTMAN did not response HEADER application/json.
	err = json.Unmarshal(resp.Body(), dataResp)
	if err != nil {
		return err
	}

	return nil
}
