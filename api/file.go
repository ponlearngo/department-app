package api

import (
	"bufio"
	"department-app/bindings"
	"department-app/custom_configuration"
	"encoding/json"
	"fmt"
	"mime/multipart"
	"net/http"

	"github.com/go-resty/resty/v2"
	"gitlab.com/gecthai/enginex/common_renderings"
)

const (
	uploadFilesEndpoint = "upload"
)

func getFileServiceUrl() (string, error) {
	cfg, err := custom_configuration.Config()
	if err != nil {
		return "", err
	}

	return cfg.IdentityServiceUrl, nil
}

func UploadFile(request *bindings.UploadFilesRequest, form *multipart.Form) (*common_renderings.Response, error) {
	getUrl, err := getIdentityServiceUrl()
	if err != nil {
		return nil, err
	}

	endpoint := fmt.Sprintf("%s%s", getUrl, uploadFilesEndpoint)

	dataResp := &common_renderings.Response{}

	formData := map[string]string{
		"files_length":  fmt.Sprintf("%d", request.ImagesLength),
		"images_length": fmt.Sprintf("%d", request.FilesLength),
	}

	// Create a Resty Client
	client := resty.New()
	clientRequest := client.R()
	if request.FilesLength > 0 {
		for i := 0; i < request.FilesLength; i++ {
			var fileHeader *multipart.FileHeader
			var fileData multipart.File
			formFileName := fmt.Sprintf("file_doc%d", i)
			file := form.File[formFileName]
			if len(formFileName) > 0 {
				fileHeader = file[0]
			}
			if fileHeader != nil {
				fileDocs, err := fileHeader.Open()
				if err != nil {
					return nil, err
				}
				fileData = fileDocs
				defer fileDocs.Close()
			}
			clientRequest = clientRequest.SetFileReader(formFileName, fileHeader.Filename, bufio.NewReader(fileData))
		}
	}
	if request.ImagesLength > 0 {
		for i := 0; i < request.ImagesLength; i++ {
			var fileHeader *multipart.FileHeader
			var fileData multipart.File
			formFileName := fmt.Sprintf("file_image%d", i)
			file := form.File[formFileName]
			if len(formFileName) > 0 {
				fileHeader = file[0]
			}
			if fileHeader != nil {
				fileImage, err := fileHeader.Open()
				if err != nil {
					return nil, err
				}
				fileData = fileImage
				defer fileImage.Close()
			}
			clientRequest = clientRequest.SetFileReader(formFileName, fileHeader.Filename, bufio.NewReader(fileData))
		}
	}

	clientRequest.SetFormData(formData)
	resp, err := clientRequest.Post(endpoint)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("api error %d: %s", resp.StatusCode(), string(resp.Body()))
	}

	err = json.Unmarshal(resp.Body(), dataResp)
	if err != nil {
		return nil, err
	}

	return dataResp, nil
}
