package renderings

type Personal struct {
	PersonalCode string `json:"personal_code"`
	Name         string `json:"name"`
	LastName     string `json:"last_name"`
}
