package renderings

type UserData struct {
	UserId          string                 `json:"user_id"`
	UserName        string                 `json:"user_name"`
	UserEmail       string                 `json:"user_email"`
	IsAdmin         bool                   `json:"is_admin"`
	FeatureFunction map[string]interface{} `json:"feature_function"`
}
