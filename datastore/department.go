package datastore

import (
	"database/sql"
	"department-app/bindings"
	"fmt"
	"strings"

	sq "github.com/Masterminds/squirrel"
	log "gitlab.com/gecthai/enginex/echo_logrus"
	"gitlab.com/gecthai/enginex/util/stringutil"
)

type Department struct {
	DepartmentId   string `json:"department_id"`
	DepartmentName string `json:"department_name"`
	Address        string `json:"address"`
	Email          string `json:"email"`
	IsDeleted      bool   `json:"is_deleted"`
}

func (department Department) String() string {
	return stringutil.JsonPretty(department)
}

func GetDepartmentList(db *sql.DB) ([]*Department, int64, error) {
	var totals int64 = 0

	results := make([]*Department, 0)

	sqlCmd := `SELECT * FROM public.department
	ORDER BY "departmentName" ASC LIMIT 100`

	rows, err := db.Query(
		sqlCmd,
	)

	if err != nil {
		return nil, totals, err
	}

	defer rows.Close()
	for rows.Next() {
		item := Department{}
		if err := rows.Scan(
			&item.DepartmentId,
			&item.DepartmentName,
			&item.Address,
			&item.Email,
			&item.IsDeleted,
		); err != nil {
			log.Errorf("datastore.GetDepartmentList: %s", err.Error())
			return nil, totals, err
		}
		results = append(results, &item)
	}
	totals = int64(len(results))
	return results, totals, nil

}

func InstertDepartment(conn *sql.DB, dep *Department) (*Department, int64, error) {

	tx, err := conn.Begin()
	if err != nil {
		return nil, 0, err
	}

	defer tx.Rollback()

	sqlCmd := `INSERT INTO public.department ("departmentName", "address", "email", "isDeleted") ` +
		"VALUES ($1, $2, $3, $4);"

	if result, err := tx.Exec(sqlCmd,
		dep.DepartmentName,
		dep.Address,
		dep.Email,
		0,
	); err != nil {
		return nil, 0, err
	} else {
		if commitErr := tx.Commit(); commitErr != nil {
			return nil, 0, err
		}
		total, _ := result.RowsAffected()
		return dep, total, nil
	}
}

func GetDepartmentBuilder(db *sql.DB, filters *bindings.DepartmentSearchRequest) ([]*Department, int64, error) {
	var totals int64 = 0

	// sql1 := sq.Select("*").From("department")
	searchBuilder := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	results := make([]*Department, 0)

	psql := searchBuilder.Select("*").From("department")

	if len(filters.TextSearch) > 0 {
		psql = psql.Where(sq.Like{`lower("departmentName")`: strings.TrimSpace(fmt.Sprintf("%%%s%%", strings.ToLower(filters.TextSearch)))})
	}
	if len(filters.DepartmentName) > 0 {
		psql = psql.Where(sq.Like{`lower("departmentName")`: strings.TrimSpace(fmt.Sprintf("%%%s%%", strings.ToLower(filters.DepartmentName)))})
	}
	if len(filters.Address) > 0 {
		psql = psql.Where(sq.Like{`lower("address")`: strings.TrimSpace(fmt.Sprintf("%%%s%%", strings.ToLower(filters.Address)))})
	}
	if len(filters.Email) > 0 {
		psql = psql.Where(sq.Like{`lower("email")`: strings.TrimSpace(fmt.Sprintf("%%%s%%", strings.ToLower(filters.Email)))})
	}

	psql = psql.Limit(3)

	rows, err := psql.RunWith(db).Query()
	if err != nil {
		return nil, totals, err
	}
	defer rows.Close()
	for rows.Next() {
		item := Department{}
		if err := rows.Scan(
			&item.DepartmentId,
			&item.DepartmentName,
			&item.Address,
			&item.Email,
			&item.IsDeleted,
		); err != nil {
			log.Errorf("datastore.GetDepartmentList: %s", err.Error())
			return nil, totals, err
		}

		results = append(results, &item)
	}

	// sqlCmd := `SELECT * FROM public.department where 1=1 `

	// if len(filters.DepartmentName) > 0 {
	// 	sqlCmd = sqlCmd + fmt.Sprintf(`and  lower("departmentName") like lower('%%%s%%')`, filters.DepartmentName)

	// }
	// if len(filters.Address) > 0 {
	// 	sqlCmd = sqlCmd + fmt.Sprintf(`and  lower("address") like lower('%%%s%%')`, filters.Address)
	// }
	// if len(filters.Email) > 0 {
	// 	sqlCmd = sqlCmd + fmt.Sprintf(`and  lower("email") like lower('%%%s%%')`, filters.Email)
	// }

	// rows, err := db.Query(
	// 	sqlCmd,
	// )

	// if err != nil {
	// 	return nil, totals, err
	// }

	// defer rows.Close()
	// for rows.Next() {
	// 	item := Department{}
	// 	if err := rows.Scan(
	// 		&item.DepartmentId,
	// 		&item.DepartmentName,
	// 		&item.Address,
	// 		&item.Email,
	// 		&item.IsDeleted,
	// 	); err != nil {
	// 		log.Errorf("datastore.GetDepartmentList: %s", err.Error())
	// 		return nil, totals, err
	// 	}
	// 	results = append(results, &item)
	// }
	totals = int64(len(results))
	return results, totals, nil
}

func DeleteDepartment(conn *sql.DB, params *bindings.DepartmentDeleteRequest) ([]*Department, int64, error) {
	tx, err := conn.Begin()
	if err != nil {
		return nil, 0, err
	}

	// ids := []int64{}

	// for _, item := range params.List {
	// 	id, _ := strconv.ParseInt(item, 10, 64)
	// 	ids = append(ids,  id)
	// }
	ids := strings.Join(params.List[:], ",")

	sqlCmd := fmt.Sprintf(`DELETE FROM public.department WHERE "departmentId" in (%s);`, ids)

	if result, err := tx.Exec(sqlCmd); err != nil {
		return nil, 0, err
	} else {
		if commitErr := tx.Commit(); commitErr != nil {
			return nil, 0, err
		}
		total, _ := result.RowsAffected()
		return nil, total, nil
	}
}
