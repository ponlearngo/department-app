package bindings

import (
	"department-app/handlers/message"
	"encoding/json"
	"regexp"

	"gitlab.com/gecthai/enginex/common_renderings"
	"gitlab.com/gecthai/enginex/util/stringutil"
)

type DepartmentAddRequest struct {
	DepartmentName string `json:"department_name"`
	Email          string `json:"email"`
	Address        string `json:"address"`
}

func (req DepartmentAddRequest) String() string {
	jsonRaw, err := json.MarshalIndent(&req, " ", " ")
	if err != nil {
		return ""
	}
	return string(jsonRaw)
}

func (req *DepartmentAddRequest) Validate() error {
	errResults := common_renderings.ErrorsResponse{}
	if stringutil.IsEmptyString(req.DepartmentName) {
		errResults.Append(message.KSE00001)
	}
	if stringutil.IsEmptyString(req.Email) {
		errResults.Append(message.KSE00002)
	}

	if !isEmailValid(req.Email) {
		errResults.Append(message.KSE00003)
	}

	if stringutil.IsEmptyString(req.Address) {
		errResults.Append(message.KSE00004)
	}

	if errResults.Len() > 0 {
		return errResults
	}
	return nil
}

func isEmailValid(e string) bool {
	emailRegex := regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`)
	return emailRegex.MatchString(e)
}

type DepartmentSearchRequest struct {
	TextSearch     string `json:"text_search"`
	DepartmentName string `json:"department_name"`
	Email          string `json:"email"`
	Address        string `json:"address"`
}

func (req DepartmentSearchRequest) String() string {
	jsonRaw, err := json.MarshalIndent(&req, " ", " ")
	if err != nil {
		return ""
	}
	return string(jsonRaw)
}

type DepartmentDeleteRequest struct {
	List []string `json:"list"`
}

func (req DepartmentDeleteRequest) String() string {
	jsonRaw, err := json.MarshalIndent(&req, " ", " ")
	if err != nil {
		return ""
	}
	return string(jsonRaw)
}
