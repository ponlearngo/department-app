package bindings

import (
	"gitlab.com/gecthai/enginex/common_renderings"
)

var (
	errE00001 = common_renderings.NewError("e00001",
		"UserName is require", nil)
	errE00002 = common_renderings.NewError("e00002",
		"Password is require", nil)
)
