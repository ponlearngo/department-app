package bindings

import "encoding/json"

type ViewCreditSupportRequest struct {
	CreditCode string `json:"credit_code"`
	Type       string `json:"type"`
}

func (req ViewCreditSupportRequest) String() string {
	jsonRaw, err := json.MarshalIndent(&req, " ", " ")
	if err != nil {
		return ""
	}
	return string(jsonRaw)
}
