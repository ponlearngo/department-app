package bindings

import (
	"encoding/json"

	"gitlab.com/gecthai/enginex/common_renderings"
	"gitlab.com/gecthai/enginex/util/stringutil"
)

type LoginRequest struct {
	UserName string `json:"user_name" `
	Password string `json:"password" `
}

func (req LoginRequest) String() string {
	jsonRaw, err := json.MarshalIndent(&req, " ", " ")
	if err != nil {
		return ""
	}
	return string(jsonRaw)
}

func (req *LoginRequest) Validate() error {
	errResults := common_renderings.ErrorsResponse{}
	if stringutil.IsEmptyString(req.UserName) {
		errResults.Append(errE00001)
	}
	if stringutil.IsEmptyString(req.Password) {
		errResults.Append(errE00002)
	}
	if errResults.Len() > 0 {
		return errResults
	}
	return nil
}
