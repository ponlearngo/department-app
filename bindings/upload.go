package bindings

import "encoding/json"

type UploadFilesRequest struct {
	FilesLength  int `json:"files_length" form:"files_length"`
	ImagesLength int `json:"images_length" form:"images_length"`
}

func (req UploadFilesRequest) String() string {
	jsonRaw, err := json.MarshalIndent(&req, " ", " ")
	if err != nil {
		return ""
	}
	return string(jsonRaw)
}
