package common_renderings

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/gecthai/enginex/util/stringutil"
)

type ErrorsResponse struct {
	Errors []*Error `json:"errors,omitempty"`
}

func NewErrorsResponse(errs ...*Error) *ErrorsResponse {
	response := &ErrorsResponse{}
	for i, _ := range errs {
		response.Append(errs[i])
	}
	return response
}

func (errResp *ErrorsResponse) String() string {
	jsonRaw, err := json.MarshalIndent(errResp, " ", " ")
	if err != nil {
		return ""
	}
	return string(jsonRaw)
}

func (errResp *ErrorsResponse) Append(err *Error) *ErrorsResponse {
	if errResp.Errors == nil {
		errResp.Errors = make([]*Error, 0)
	}
	errResp.Errors = append(errResp.Errors, err)
	return errResp
}

func (errResp *ErrorsResponse) Len() int {
	return len(errResp.Errors)
}

func (errResp *ErrorsResponse) MessageCode() string {
	msgCodes := make([]string, 0)
	for _, e := range errResp.Errors {
		if stringutil.IsNotEmptyString(e.MessageCode) {
			msgCodes = append(msgCodes, e.MessageCode)
		}
	}
	return strings.Join(msgCodes, ", ")
}

func (errResp ErrorsResponse) Error() string {
	errStr := make([]string, 0)
	for _, e := range errResp.Errors {
		if stringutil.IsNotEmptyString(e.Message) {
			errStr = append(errStr, e.Message)
		}
	}
	return strings.Join(errStr, ", ")
}

type Error struct {
	MessageCode string                 `json:"message_code,omitempty"`
	Sources     map[string]interface{} `json:"sources,omitempty"`
	Message     string                 `json:"message"`
}

func (e Error) String() string {
	jsonRaw, err := json.MarshalIndent(&e, " ", " ")
	if err != nil {
		return ""
	}
	return string(jsonRaw)
}

func (err Error) Error() string {
	return fmt.Sprintf("[%s] %s", err.MessageCode, err.Message)
}

func NewError(messageCode string, message string, sources map[string]interface{}) *Error {
	return &Error{
		MessageCode: messageCode,
		Message:     message,
		Sources:     sources,
	}
}
