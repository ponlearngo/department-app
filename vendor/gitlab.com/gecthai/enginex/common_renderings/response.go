package common_renderings

import "encoding/json"

type Response struct {
	StatusCode  int         `json:"statusCode,omitempty"`
	Body        string      `json:"body,omitempty"`
	Success     bool        `json:"success"`
	MessageCode string      `json:"message_code,omitempty"`
	Message     string      `json:"message,omitempty"`
	Total       int64       `json:"total,omitempty"`
	ForwardURL  string      `json:"forward_url,omitempty"`
	Datas       interface{} `json:"datas,omitempty"`
	ErrorsResponse
}

func (resp Response) String() string {
	jsonRaw, err := json.MarshalIndent(&resp, " ", " ")
	if err != nil {
		return ""
	}
	return string(jsonRaw)
}

func (resp *Response) BindBody() {
	jsonRaw, err := json.Marshal(resp)
	if err != nil {
		resp.Body = "{}"
	}
	resp.Body = string(jsonRaw)
}
