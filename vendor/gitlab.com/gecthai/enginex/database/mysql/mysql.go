package mysql

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/gecthai/enginex/database"
	"gitlab.com/gecthai/enginex/util/stringutil"
)

func Open(url string, user string, password string, dbName string) (*sql.DB, error) {
	if stringutil.IsEmptyString(url) {
		return nil, database.ErrDBURLRequire
	}
	if stringutil.IsEmptyString(user) {
		return nil, database.ErrDBUserRequire
	}
	if stringutil.IsEmptyString(password) {
		return nil, database.ErrDBPasswordRequire
	}
	if stringutil.IsEmptyString(dbName) {
		return nil, database.ErrDBNameRequire
	}

	urlStr := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8&parseTime=True&multiStatements=true", user, password, url, dbName)
	db, err := sql.Open("mysql", urlStr)
	if err != nil {
		return nil, err
	}
	return db, nil

}
