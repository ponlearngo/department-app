package configuration

import (
	"errors"
	"fmt"

	"gitlab.com/gecthai/enginex/util/fileutil"
	"gitlab.com/gecthai/enginex/util/stringutil"
)

var (
	ErrConfWebAppResourceMappingNotfoundLocalFolder = func(folder string) error {
		return fmt.Errorf("error web resources configuration source folder %s is not exist", folder)
	}

	ErrConfWebAppResourceMappingCheckLocalFolder = func(folder string, err error) error {
		return fmt.Errorf("error web resources configuration source folder %s %s", folder, err)
	}

	ErrConfWebAppResourceMappingLocalFolderDup = func(folder string) error {
		return fmt.Errorf("error web resources configuration source folder %s duplicate", folder)
	}

	ErrConfWebAppResourceMappingTargetDup = func(target string) error {
		return fmt.Errorf("error web resources configuration target path  %s duplicate", target)
	}

	ErrConfWebAppResourceMappingTargetRequire = errors.New("error web resources configuration target path is not blank")
)

func validConfigWebAppResource(config *Configuration) error {
	if config == nil {
		return ErrorInvalidConfig
	}

	//validate source folder
	targetCount := make(map[string]int)
	sourceCount := make(map[string]int)
	for _, staticMap := range config.WebApp.Resources.ResourceMappings {
		for target, source := range staticMap {
			targetCount[target]++
			sourceCount[source]++
			isExist, err := fileutil.IsDirExist(source)
			if err != nil {
				return ErrConfWebAppResourceMappingCheckLocalFolder(source, err)
			}
			if !isExist {
				return ErrConfWebAppResourceMappingNotfoundLocalFolder(source)
			}
			if stringutil.IsEmptyString(target) {
				return ErrConfWebAppResourceMappingTargetRequire
			}

		}
	}

	//valid duplicate
	for target, count := range targetCount {
		if count > 1 {
			return ErrConfWebAppResourceMappingTargetDup(target)
		}
	}

	for source, count := range sourceCount {
		if count > 1 {
			return ErrConfWebAppResourceMappingLocalFolderDup(source)
		}
	}

	return nil
}
