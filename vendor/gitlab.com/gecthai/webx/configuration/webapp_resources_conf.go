package configuration

import (
	"encoding/json"
)

type WebAppResourcesConfig struct {
	ResourcePathPrefix string              `mapstructure:"resource-path-prefix" json:"resource_path_prefix"`
	ResourceMappings   []map[string]string `mapstructure:"resources-mapping" json:"resource_mappings"`
	DirList            bool                `mapstructure:"directory-listing" json:"dir_list"`
}

func (cfg WebAppResourcesConfig) String() string {
	jsonRaw ,err := json.MarshalIndent(&cfg, " ", " ")
	if err != nil{
		return ""
	}
	return string(jsonRaw)
}
