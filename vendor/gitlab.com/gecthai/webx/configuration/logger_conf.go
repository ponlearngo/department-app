package configuration

import (
	"encoding/json"
)

const (
	LogLevelDebug = "debug"
	LogLevelInfo = "info"
	LogLevelWarn = "warn"
	LogLevelError = "error"

	LogFormatText = "text"
	LogFormatJson = "json"


)

type LogConfig struct {
	Level  string `json:"level"`
	Format string `json:"format"`
}

func (cfg LogConfig) String() string {
	jsonRaw ,err := json.MarshalIndent(&cfg, " ", " ")
	if err != nil{
		return ""
	}
	return string(jsonRaw)
}

