package configuration

import (
	"encoding/json"
)

type WebAppConfig struct {
	Resources  WebAppResourcesConfig `json:"resources"`
	Port       int                   `json:"port"`
	HealthPort int                   `json:"health_port"`
	//>= periodSeconds * failureThreshold
	K8SZeroDownTimeThreshold int `json:"k_8_s_zero_down_time_threshold"`
	//default > 10 
	GracefulShutdownTimeout int                 `json:"graceful_shutdown_timeout"`
	CORs                    WebAppCORsConfig    `json:"co_rs"`
	CSRF                    WebAppCSRFConfig    `json:"csrf"`
	SessionStore            WebAppSessionConfig `json:"session_store"`
	Databases               []WebAppDBConfig    `json:"databases"`
}

func (cfg WebAppConfig) String() string {
	jsonRaw, err := json.MarshalIndent(&cfg, " ", " ")
	if err != nil {
		return ""
	}
	return string(jsonRaw)
}
