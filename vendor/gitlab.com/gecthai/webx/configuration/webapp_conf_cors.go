package configuration

import (
	"encoding/json"
	"github.com/labstack/echo"
	"net/http"
)

var (
	defaultAllowOrigins = []string{"*"}

	defaultAllowMethods = []string{
		http.MethodGet,
		http.MethodHead,
		http.MethodPut,
		http.MethodPatch,
		http.MethodPost,
		http.MethodDelete,
	}

	defaultAllowHeader = []string{
		echo.HeaderOrigin,
		echo.HeaderContentType,
		echo.HeaderAccept,
		echo.HeaderAcceptEncoding,
		echo.HeaderAccessControlAllowHeaders,
		echo.HeaderAccessControlAllowMethods,
		echo.HeaderAccessControlAllowOrigin,
		echo.HeaderXRequestedWith,
		echo.HeaderAuthorization,
		echo.HeaderXCSRFToken,
	}

	defaultExposeHeaders = []string{}

	defaultMaxAge = 0
)

type WebAppCORsConfig struct {
	AllowOrigins     [] string `json:"allow_origins"`
	AllowMethods     [] string `json:"allow_methods"`
	AllowHeaders     [] string `json:"allow_headers"`
	AllowCredentials bool `json:"allow_credentials"`
	ExposeHeaders    []string `json:"expose_headers"`
	MaxAge           int `json:"max_age"`
}

func (cfg WebAppCORsConfig) String() string {
	jsonRaw ,err := json.MarshalIndent(&cfg, " ", " ")
	if err != nil{
		return ""
	}
	return string(jsonRaw)
}
