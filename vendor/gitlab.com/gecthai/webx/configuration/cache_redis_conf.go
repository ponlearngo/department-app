package configuration

import (
	"encoding/json"
)

type RedisCacheConfig struct{
	CacheName string `json:"cache_name"`
	RedisMaxIdle int `json:"redis_max_idle"`
	RedisURL string `json:"redis_url"`
	RedisPassword string `json:"redis_password"`
	MaxLength int `json:"max_length"`
	CreateConnectionTimeout int `json:"create_connection_timeout"`
}

func (cfg RedisCacheConfig) String() string {
	jsonRaw ,err := json.MarshalIndent(&cfg, " ", " ")
	if err != nil{
		return ""
	}
	return string(jsonRaw)
}
