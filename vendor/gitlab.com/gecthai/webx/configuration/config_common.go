package configuration

import "errors"

const (
	EnvKeyServerConfigFileName  = "SERVER_CONFIG_NAME"
	DefaultServerConfigFileName = "server"
	EnvKeyCustomConfigFileName  = "CUSTOM_CONFIG_NAME"
	DefaultCustomConfigFileName = "custom"
	EnvKeyAppSecret             = "APP_SECRET_KEY"
)

type AppConfig struct {
	*Configuration
}

//configuration
var singletonConfig *AppConfig = nil

func Config() (*AppConfig, error) {
	if singletonConfig == nil {
		config, err := read()
		if err != nil {
			return nil, err
		}
		singletonConfig = &AppConfig{
			Configuration: config,
		}
	}

	return singletonConfig, nil
}

func Reload() (*AppConfig, error) {
	config, err := read()
	if err != nil {
		return nil, err
	}
	singletonConfig = &AppConfig{
		Configuration: config,
	}
	return singletonConfig, nil

}

var (
	ErrorInvalidConfig = errors.New("invalid configuration")
)

type ValidConfigurationFunc func(*Configuration) error

var validConfigValidFuncs = []ValidConfigurationFunc{
	validConfigWebAppResource,
	validConfigWebAppPort,
	validConfigWebAppHealthzPort,
	validConfigWebAppDB,
	validConfigLog,
	validConfigWebAppRedisSession,
	validConfigRedisCache,
}
