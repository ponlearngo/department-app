package configuration

import (
	"encoding/json"
)

const (
	POSTGRES = "postgres"
	MYSQL    = "mysql"
)

type WebAppDBConfig struct {
	ContextName             string `json:"context_name"`
	Provider                string `json:"provider"`
	URL                     string `json:"url"`
	User                    string `json:"user"`
	Password                string `json:"password"`
	DatabaseName            string `json:"database_name"`
	CreateConnectionTimeout int    `json:"create_connection_timeout"`
	InitialScripts             []string `json:"initial_scripts"`
	MySQLConnectionMaxLifeTime int      `mapstructure:"mysql-connection-max-life-time" json:"my_sql_connection_max_life_time"`
	MySQLSetMaxIdleConns       int      `mapstructure:"mysql-set-max-idle-conns" json:"my_sql_set_max_idle_conns"`
	MySQLSetMaxOpenConns       int      `mapstructure:"mysql-set-max-open-conns" json:"my_sql_set_max_open_conns"`
}

func (cfg *WebAppDBConfig) String() string {
	jsonRaw, err := json.MarshalIndent(cfg, " ", " ")
	if err != nil {
		return ""
	}
	return string(jsonRaw)
}
