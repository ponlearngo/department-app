package configuration

import (
	"encoding/json"
)

type WebAppSessionConfig struct {
	RedisStores []WebAppSessionRedisConfig `json:"redis_stores"`
}

type WebAppSessionRedisConfig struct {
	SessionName             string `json:"session_name"`
	RedisMaxIdle            int    `json:"redis_max_idle"`
	RedisURL                string `json:"redis_url"`
	RedisPassword           string `json:"redis_password"`
	MaxAge                  int    `json:"max_age"`
	MaxLength               int    `json:"max_length"`
	CreateConnectionTimeout int    `json:"create_connection_timeout"`
	HttpOnly                bool   `json:"http_only"`
	Secure                  bool   `json:"secure"`
	SameSite                int    `json:"same_site"`
}

func (cfg WebAppSessionRedisConfig) String() string {
	jsonRaw, err := json.MarshalIndent(&cfg, " ", " ")
	if err != nil {
		return ""
	}
	return string(jsonRaw)
}
