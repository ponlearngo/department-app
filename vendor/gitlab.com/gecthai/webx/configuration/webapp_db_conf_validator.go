package configuration

import (
	"errors"
	"fmt"

	"gitlab.com/gecthai/enginex/util/stringutil"
)

var DBProviders = map[string]bool{
	POSTGRES: true,
	MYSQL:    true,
}

var (
	ErrConfWebAppDBInvalidDBProvider = func(contextName string, provider string) error {
		return fmt.Errorf("error web database configuration invalid provider %s at context name %s", provider, contextName)
	}

	ErrConfWebAppDBDuplicateContextName = func(contextName string) error {
		return fmt.Errorf("error database context name  %s duplicate", contextName)
	}

	ErrConfWebAppDBContextRequire = errors.New("error web database configuration context name is require")

	ErrConfWebAppDBProviderRequire = func(contextName string) error {
		return fmt.Errorf("error web database configuration provider at context name %s is require", contextName)
	}

	ErrConfWebAppDBURLRequire = func(contextName string) error {
		return fmt.Errorf("error web database configuration url at context name %s is require", contextName)
	}

	ErrConfWebAppDBUserRequire = func(contextName string) error {
		return fmt.Errorf("error web database configuration user at context name %s is require", contextName)
	}

	ErrConfWebAppDBPasswordRequire = func(contextName string) error {
		return fmt.Errorf("error web database configuration password at context name %s is require", contextName)
	}

	ErrConfWebAppDBDatabaseNameRequire = func(contextName string) error {
		return fmt.Errorf("error web database configuration password at database name %s is require", contextName)
	}
)

func validConfigWebAppDB(config *Configuration) error {
	if config == nil {
		return ErrorInvalidConfig
	}
	contextNameCount := make(map[string]int)
	for _, database := range config.WebApp.Databases {
		if stringutil.IsEmptyString(database.ContextName) {
			return ErrConfWebAppDBContextRequire
		}
		contextNameCount[database.ContextName]++
		if contextNameCount[database.ContextName] > 1 {
			return ErrConfWebAppDBDuplicateContextName(database.ContextName)
		}
		if stringutil.IsEmptyString(database.Provider) {
			return ErrConfWebAppDBProviderRequire(database.ContextName)
		}
		if !DBProviders[database.Provider] {
			return ErrConfWebAppDBInvalidDBProvider(database.ContextName, database.Provider)
		}
		if stringutil.IsEmptyString(database.URL) {
			return ErrConfWebAppDBURLRequire(database.ContextName)
		}
		if stringutil.IsEmptyString(database.User) {
			return ErrConfWebAppDBUserRequire(database.ContextName)
		}
		if stringutil.IsEmptyString(database.Password) {
			return ErrConfWebAppDBPasswordRequire(database.ContextName)
		}
		if stringutil.IsEmptyString(database.DatabaseName) {
			return ErrConfWebAppDBDatabaseNameRequire(database.ContextName)
		}

	}
	return nil
}
