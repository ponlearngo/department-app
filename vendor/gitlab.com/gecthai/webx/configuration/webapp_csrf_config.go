package configuration

import (
	"encoding/json"
)

type WebAppCSRFConfig struct {
	CookieName string `json:"cookie_name"`
	CookiePath string `json:"cookie_path"`
	//in second
	CookieMaxAge       int  `json:"cookie_max_age"`
	CookieSecure       bool `json:"cookie_secure"`
	CookieHTTPOnly     bool `json:"cookie_http_only"`
	CookieSameSiteMode int  `json:"cookie_same_site_mode"`
	Skip               bool `json:"skip"`
}

func (cfg WebAppCSRFConfig) String() string {
	jsonRaw, err := json.MarshalIndent(&cfg, " ", " ")
	if err != nil {
		return ""
	}
	return string(jsonRaw)
}
