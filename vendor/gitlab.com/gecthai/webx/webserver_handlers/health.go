package webserver_handlers

import (
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/gecthai/enginex/common_renderings"
)

const (
	HealthyStatus   = "healthy"
	UnHealthyStatus = "un-healthy"
)

func HealthCheck(c echo.Context) error {
	resp := common_renderings.Response{
		StatusCode: http.StatusOK,
		Message:    HealthyStatus,
		Success:    true,
	}
	resp.BindBody()
	return c.JSON(http.StatusOK, &resp)
}
