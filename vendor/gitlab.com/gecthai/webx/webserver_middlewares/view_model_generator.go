package webserver_middlewares

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/gecthai/webx/webserver_constant"
)

func ViewModelGenerator() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			vm := make(map[string]interface{})
			if csrfToken, ok := c.Get(middleware.DefaultCSRFConfig.ContextKey).(string); ok {
				vm[webserver_constant.CsrfTokenViewModelKey] = csrfToken
			}
			if len(vm) > 0 {
				c.Set(webserver_constant.ViewModelContextKey, vm)
			}
			return next(c)
		}
	}
}
