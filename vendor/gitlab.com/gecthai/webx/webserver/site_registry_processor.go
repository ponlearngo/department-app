package webserver

import (
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/gecthai/enginex/server_constant"
	"gitlab.com/gecthai/enginex/server_middlewares"
	"gitlab.com/gecthai/enginex/session"
	"gitlab.com/gecthai/enginex/util/stringutil"
	"gitlab.com/gecthai/webx/webserver_constant"
	"gitlab.com/gecthai/webx/webserver_handlers"
	"gitlab.com/gecthai/webx/webserver_middlewares"
)

func skipFunc(isSkip bool) middleware.Skipper {
	return func(c echo.Context) bool {
		return isSkip
	}
}

func (ws *WebServer) handlerRegister() error {

	var baseMiddleWares = map[string]echo.MiddlewareFunc{
		server_constant.SecureMiddleware: middleware.SecureWithConfig(middleware.SecureConfig{
			XSSProtection:      "1; mode=block",
			ContentTypeNosniff: "nosniff",
			XFrameOptions:      "SAMEORIGIN",
			HSTSMaxAge:         3600,
		}),

		server_constant.CSRFMiddleware: server_middlewares.CSRFWithConfig(server_middlewares.CSRFConfig{
			CookieName:     ws.config.WebApp.CSRF.CookieName,
			CookieMaxAge:   ws.config.WebApp.CSRF.CookieMaxAge,
			CookieSecure:   ws.config.WebApp.CSRF.CookieSecure,
			CookieHTTPOnly: ws.config.WebApp.CSRF.CookieHTTPOnly,
			CookiePath:     ws.config.WebApp.CSRF.CookiePath,
			Skipper:        skipFunc(ws.config.WebApp.CSRF.Skip),
			CookieSameSite: http.SameSite(ws.config.WebApp.CSRF.CookieSameSiteMode),
		}),

		server_constant.CSRFIncludeGETMethodMiddleware: server_middlewares.CSRFIncludeGETMethodWithConfig(server_middlewares.CSRFConfig{
			CookieName:     ws.config.WebApp.CSRF.CookieName,
			CookieMaxAge:   ws.config.WebApp.CSRF.CookieMaxAge,
			CookieSecure:   ws.config.WebApp.CSRF.CookieSecure,
			CookieHTTPOnly: ws.config.WebApp.CSRF.CookieHTTPOnly,
			CookiePath:     ws.config.WebApp.CSRF.CookiePath,
			TokenLookup:    server_constant.DefaultCSRFGetMethodTokenLookup,
			Skipper:        skipFunc(ws.config.WebApp.CSRF.Skip),
			CookieSameSite: http.SameSite(ws.config.WebApp.CSRF.CookieSameSiteMode),
		}),

		server_constant.CORSMiddleware: middleware.CORSWithConfig(middleware.CORSConfig{
			AllowOrigins:     ws.config.WebApp.CORs.AllowOrigins,
			AllowMethods:     ws.config.WebApp.CORs.AllowMethods,
			AllowHeaders:     ws.config.WebApp.CORs.AllowHeaders,
			AllowCredentials: ws.config.WebApp.CORs.AllowCredentials,
			ExposeHeaders:    ws.config.WebApp.CORs.ExposeHeaders,
			MaxAge:           ws.config.WebApp.CORs.MaxAge,
		}),

		server_constant.DBContextAppenderMiddleware:     server_middlewares.DBContextAppender(ws.dbConnections),
		server_constant.CacheStoreAppenderMiddleware:    server_middlewares.CacheStoreAppender(ws.cacheStoreConnections),
		server_constant.NoCacheMiddleware:               server_middlewares.NoCache,
		server_constant.UUIDSessionGeneratorMiddleware:  server_middlewares.UUIDGenerator(ws.sessionStores),
		webserver_constant.ViewModelGeneratorMiddleware: webserver_middlewares.ViewModelGenerator(),
	}

	defaultPageMiddleWares := []string{
		server_constant.SecureMiddleware,
		server_constant.CSRFMiddleware,
		server_constant.CORSMiddleware,
		server_constant.NoCacheMiddleware,
		server_constant.DBContextAppenderMiddleware,
		server_constant.CacheStoreAppenderMiddleware,
		webserver_constant.ViewModelGeneratorMiddleware,
	}

	defaultAPIMiddleWares := []string{
		server_constant.SecureMiddleware,
		server_constant.CSRFMiddleware,
		server_constant.CORSMiddleware,
		server_constant.CacheStoreAppenderMiddleware,
		server_constant.DBContextAppenderMiddleware,
	}

	if ws.website == nil {
		return ErrSiteRegistryRequire
	}

	//generate session store middleware

	sessionMiddleWares := make([]echo.MiddlewareFunc, 0)

	for sessionName, store := range ws.sessionStores {
		if stringutil.IsEmptyString(sessionName) {
			return ErrSessionNameReq
		}

		if store == nil {
			return ErrSessionStoreReq
		}

		sessionMiddleWares = append(sessionMiddleWares, session.Sessions(sessionName, store))
	}

	//register base pageAPI
	for _, baseWebPage := range ws.website.BaseWebPages {
		for _, pageAPI := range baseWebPage.PageAPIs {
			//generate base page api middleware
			apiMiddleWares := make([]echo.MiddlewareFunc, 0)
			if len(pageAPI.ServerAPIMiddleWares) <= 0 {
				for _, middleWareName := range defaultAPIMiddleWares {
					apiMiddleWares = append(apiMiddleWares, baseMiddleWares[middleWareName])
				}
			} else {
				for _, middleWareName := range pageAPI.ServerAPIMiddleWares {
					apiMiddleWares = append(apiMiddleWares, baseMiddleWares[middleWareName])
				}
			}
			if len(sessionMiddleWares) > 0 {
				apiMiddleWares = append(apiMiddleWares, sessionMiddleWares...)
				apiMiddleWares = append(apiMiddleWares, baseMiddleWares[server_constant.UUIDSessionGeneratorMiddleware])
			}
			apiMiddleWares = append(apiMiddleWares, pageAPI.MiddleWares...)

			if len(pageAPI.URLs) > 0 {
				for _, url := range pageAPI.URLs {
					for _, method := range pageAPI.Methods {
						ws.engine.Add(method, url, pageAPI.Handler, apiMiddleWares...)

					}
				}
			}
		}

	}

	//register web page handler
	for _, webPage := range ws.website.WebPages {
		pageMiddleWares := make([]echo.MiddlewareFunc, 0)
		if len(webPage.ServerPageMiddleWares) <= 0 {
			if !webPage.SkipDefaultServerAPIMiddleWares {
				for _, middleWareName := range defaultPageMiddleWares {
					pageMiddleWares = append(pageMiddleWares, baseMiddleWares[middleWareName])
				}
			}
		} else {
			for _, middleWareName := range webPage.ServerPageMiddleWares {
				pageMiddleWares = append(pageMiddleWares, baseMiddleWares[middleWareName])
			}
		}
		if len(sessionMiddleWares) > 0 {
			pageMiddleWares = append(pageMiddleWares, sessionMiddleWares...)
			pageMiddleWares = append(pageMiddleWares, baseMiddleWares[server_constant.UUIDSessionGeneratorMiddleware])
		}
		pageMiddleWares = append(pageMiddleWares, webPage.MiddleWares...)
		if len(webPage.URLs) > 0 {
			for _, url := range webPage.URLs {
				for _, method := range webPage.Methods {
					ws.engine.Add(method, url, webPage.PageHandler, pageMiddleWares...)
				}
			}
		}

	}

	//register web page pageAPI
	for _, webPage := range ws.website.WebPages {
		for _, pageAPI := range webPage.PageAPIs {
			apiMiddleWares := make([]echo.MiddlewareFunc, 0)
			if len(pageAPI.ServerAPIMiddleWares) <= 0 {
				if !pageAPI.SkipDefaultServerAPIMiddleWares {
					for _, middleWareName := range defaultAPIMiddleWares {
						apiMiddleWares = append(apiMiddleWares, baseMiddleWares[middleWareName])
					}
				}
			} else {
				for _, middleWareName := range pageAPI.ServerAPIMiddleWares {
					apiMiddleWares = append(apiMiddleWares, baseMiddleWares[middleWareName])
				}
			}
			if len(sessionMiddleWares) > 0 {
				apiMiddleWares = append(apiMiddleWares, sessionMiddleWares...)
				apiMiddleWares = append(apiMiddleWares, baseMiddleWares[server_constant.UUIDSessionGeneratorMiddleware])
			}
			apiMiddleWares = append(apiMiddleWares, pageAPI.MiddleWares...)
			if len(pageAPI.URLs) > 0 {
				for _, url := range pageAPI.URLs {
					for _, method := range pageAPI.Methods {
						ws.engine.Add(method, url, pageAPI.Handler, apiMiddleWares...)

					}
				}
			}
		}
	}

	//register health check
	ws.engine.GET("/healthz", webserver_handlers.HealthCheck)
	return nil
}
