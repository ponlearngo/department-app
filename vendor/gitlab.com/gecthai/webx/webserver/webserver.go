package webserver

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/gecthai/enginex/database"
	"gitlab.com/gecthai/enginex/database/mysql"
	"gitlab.com/gecthai/enginex/database/postgres"
	log "gitlab.com/gecthai/enginex/echo_logrus"
	"gitlab.com/gecthai/enginex/redisstore"
	"gitlab.com/gecthai/enginex/server_middlewares"
	"gitlab.com/gecthai/enginex/session"
	"gitlab.com/gecthai/enginex/util/rdsutil"
	"gitlab.com/gecthai/webx/configuration"
)

var instant *WebServer

func Instant() *WebServer {
	return instant
}

type WebServer struct {
	engine                *echo.Echo
	dbConnections         database.Connections
	sessionStores         session.Stores
	cacheStoreConnections redisstore.CacheStoreConnections
	website               *SiteRegistry
	config                *configuration.AppConfig
	pageNotFoundHandler   echo.HandlerFunc
}

func New(opts ...WebServerOption) (*WebServer, error) {
	if instant != nil {
		err := errors.New("server is already instantiate cannot re-instantiate again")
		log.Warnf("%s", err)
		return instant, err
	}
	//force load config
	config, err := configuration.Config()
	if err != nil {
		return nil, err
	}
	server := &WebServer{
		config: config,
	}

	for _, setter := range opts {
		if setter != nil {
			err := setter(server)
			if err != nil {
				return nil, err
			}
		}
	}

	//setup page notfound hander
	if server.pageNotFoundHandler != nil {
		echo.NotFoundHandler = server.pageNotFoundHandler
	}

	server.engine = echo.New()
	server.engine.Validator = &Validator{}
	server.engine.HideBanner = true

	//common middleware
	server.engine.Logger = log.Logger()
	server.engine.Use(server_middlewares.Logger())
	server.engine.Use(middleware.Recover())
	server.engine.Use(middleware.Gzip())

	//generate exclude path

	server.engine.Use(middleware.StaticWithConfig(middleware.StaticConfig{
		Browse: config.WebApp.Resources.DirList,
	}))

	//setup database
	server.dbConnections = make(database.Connections)
	for _, webappDBCfg := range config.WebApp.Databases {

		maxCreateTimeout := webappDBCfg.CreateConnectionTimeout
		repeatTime := 0
	dbContextLoop:
		for {
			switch webappDBCfg.Provider {
			case configuration.POSTGRES:
				dbConn, err := postgres.Open(webappDBCfg.URL,
					webappDBCfg.User,
					webappDBCfg.Password,
					webappDBCfg.DatabaseName)
				if dbConn != nil {
					err = dbConn.Ping()
				} else {
					err = errors.New("cannot create postgres database connection")
				}
				if err != nil && repeatTime >= maxCreateTimeout {
					return nil, fmt.Errorf("db context name: %s %s", webappDBCfg.ContextName, err.Error())
				} else if err != nil && repeatTime < maxCreateTimeout {
					log.Warnf("db context name: %s fail %s", webappDBCfg.ContextName, err)
					time.Sleep(1 * time.Second)
					repeatTime++
					log.Warnf("db context name: %s reconnect %d time..", webappDBCfg.ContextName, repeatTime)
					continue
				}
				server.dbConnections[webappDBCfg.ContextName] = dbConn

				//execute initial script
				if len(webappDBCfg.InitialScripts) > 0 {
					err := dbInit(webappDBCfg.InitialScripts, webappDBCfg.ContextName, dbConn)
					if err != nil {
						return nil, fmt.Errorf("db context name: %s initial script fail: %s", webappDBCfg.ContextName, err.Error())
					}
				}

				break dbContextLoop
			case configuration.MYSQL:
				dbConn, err := mysql.Open(webappDBCfg.URL,
					webappDBCfg.User,
					webappDBCfg.Password,
					webappDBCfg.DatabaseName)
				if dbConn != nil {
					dbConn.SetConnMaxLifetime(time.Second * time.Duration(webappDBCfg.MySQLConnectionMaxLifeTime))
					dbConn.SetMaxIdleConns(webappDBCfg.MySQLSetMaxIdleConns)
					dbConn.SetMaxOpenConns(webappDBCfg.MySQLSetMaxOpenConns)
					err = dbConn.Ping()
				} else {
					err = errors.New("cannot create mysql database connection")
				}
				if err != nil && repeatTime >= maxCreateTimeout {
					return nil, fmt.Errorf("db context name: %s %s", webappDBCfg.ContextName, err.Error())
				} else if err != nil && repeatTime < maxCreateTimeout {
					log.Warnf("db context name: %s fail %s", webappDBCfg.ContextName, err)
					time.Sleep(1 * time.Second)
					repeatTime++
					log.Warnf("db context name: %s reconnect %d time..", webappDBCfg.ContextName, repeatTime)
					continue
				}
				server.dbConnections[webappDBCfg.ContextName] = dbConn
				//execute initial script
				if len(webappDBCfg.InitialScripts) > 0 {
					err := dbInit(webappDBCfg.InitialScripts, webappDBCfg.ContextName, dbConn)
					if err != nil {
						return nil, fmt.Errorf("db context name: %s initial script fail: %s", webappDBCfg.ContextName, err.Error())
					}
				}
				break dbContextLoop
			}
		}

	}

	//create redis session store from configuration
	server.sessionStores = make(session.Stores)
	for _, redisSession := range config.WebApp.SessionStore.RedisStores {
		if redisSession.RedisMaxIdle < 10 {
			redisSession.RedisMaxIdle = 10
		}

		var redisMaxAge int
		var redisMaxLength int
		//set max age
		if redisSession.MaxAge*60 < 60 || redisSession.MaxAge*60 > 24*60*60 {
			redisMaxAge = 60
		} else {
			redisMaxAge = redisSession.MaxAge * 60
		}

		//set max length
		if redisSession.MaxLength*1024 < 4*1024 || redisSession.MaxLength*1024 > 250*1024*1024 {
			redisMaxLength = 4 * 1024
		} else {
			redisMaxLength = redisSession.MaxLength * 1024 * 1024
		}

		maxCreateTimeout := redisSession.CreateConnectionTimeout
		repeatTime := 0

	sessionLoop:
		for {

			var store session.RedisStore
			var err error

			if redisSession.HttpOnly || redisSession.Secure {
				store, err = session.NewRedisStoreWithSecret(redisSession.RedisMaxIdle,
					redisMaxAge, redisMaxLength,
					"tcp", redisSession.RedisURL,
					redisSession.RedisPassword, redisSession.HttpOnly, redisSession.Secure, http.SameSite(redisSession.SameSite), []byte(config.SecretKey))

			} else {
				store, err = session.NewRedisStore(redisSession.RedisMaxIdle,
					redisMaxAge, redisMaxLength,
					"tcp", redisSession.RedisURL,
					redisSession.RedisPassword, http.SameSite(redisSession.SameSite), []byte(config.SecretKey))

			}

			if err != nil && repeatTime >= maxCreateTimeout {
				return nil, fmt.Errorf("redis session name: %s %s", redisSession.SessionName, err)
			} else if err != nil && repeatTime < maxCreateTimeout {
				log.Warnf("redis session name: %s fail %s", redisSession.SessionName, err)
				time.Sleep(1 * time.Second)
				repeatTime++
				log.Warnf("redis session name: %s reconnect %d time..", redisSession.SessionName, repeatTime)
				continue
			}

			server.sessionStores[redisSession.SessionName] = store
			break sessionLoop
		}

	}

	//create redis cache configuration
	redisCacheStoreConns := make(redisstore.CacheStoreConnections)
	for _, cache := range config.RedisCaches {
		if cache.RedisMaxIdle < 10 {
			cache.RedisMaxIdle = 10
		}

		var redisCacheMaxLength int

		//set max length
		if cache.MaxLength*1024 < 4*1024 || cache.MaxLength*1024 > 250*1024*1024 {
			redisCacheMaxLength = 4 * 1024
		} else {
			redisCacheMaxLength = cache.MaxLength * 1024 * 1024
		}

		maxCreateTimeout := cache.CreateConnectionTimeout
		repeatTime := 0

	cacheLoop:
		for {

			cacheStore, err := redisstore.NewRedisCacheStore(
				cache.RedisMaxIdle,
				redisCacheMaxLength,
				"tcp",
				cache.RedisURL,
				cache.RedisPassword)

			if err != nil && repeatTime >= maxCreateTimeout {
				return nil, fmt.Errorf("redis cache name: %s %s", cache.CacheName, err)
			} else if err != nil && repeatTime < maxCreateTimeout {
				log.Logger().Warnf("redis cache name: %s fail %s", cache.CacheName, err)
				time.Sleep(1 * time.Second)
				repeatTime++
				log.Logger().Warnf("redis cache name: %s reconnect %d time..", cache.CacheName, repeatTime)
				continue
			}

			redisCacheStoreConns[cache.CacheName] = cacheStore
			break cacheLoop
		}

	}
	if len(redisCacheStoreConns) > 0 {
		server.cacheStoreConnections = redisCacheStoreConns
	}

	//route resources
	for _, resources := range config.WebApp.Resources.ResourceMappings {
		for prefix, root := range resources {
			server.engine.Static(prefix, root)
		}
	}

	//validate server opt
	if server.website == nil {
		return nil, ErrSiteRegistryRequire
	}

	//template setup
	if server.website != nil {
		templates, err := server.templateGenerator()
		if err != nil {
			return nil, err
		}
		server.engine.Renderer = &TemplateRegistry{
			templates: templates,
		}

		err = server.handlerRegister()
		if err != nil {
			return nil, err
		}

		//setup mapping resource from base and web page
		for _, baseWebPage := range server.website.BaseWebPages {
			for prefix, root := range baseWebPage.FileResourceMappings {
				server.engine.File(prefix, root)
			}
		}

		//setup mapping resource from base and web page
		for _, webPage := range server.website.WebPages {
			for prefix, root := range webPage.FileResourceMappings {
				server.engine.File(prefix, root)
			}
		}
	}

	instant = server
	return server, nil
}

func (ws *WebServer) Start() error {

	//K8S Zero-Downtime Rolling and gracefully shutdown

	//validate engine
	if ws.Engine() == nil {
		return errors.New("error web server engine was not created")
	}

	//user for health check handler
	var (
		ready   = true
		muReady sync.RWMutex
	)

	//read config
	config, err := configuration.Config()
	if err != nil {
		return err
	}

	go func() {
		err := ws.Engine().Start(fmt.Sprintf(":%d", config.WebApp.Port))
		if err != nil {
			if err == http.ErrServerClosed {
				log.Info("shutting down the web server ...")
			} else {
				log.Fatal(err)
			}
		}
	}()

	// health check
	go func() {
		healthServerMux := http.NewServeMux()

		healthServerMux.HandleFunc("/readiness", func(w http.ResponseWriter, r *http.Request) {
			muReady.RLock()
			checkReady := ready
			muReady.RUnlock()
			if checkReady {
				w.WriteHeader(http.StatusOK)
				return
			}
			w.WriteHeader(http.StatusServiceUnavailable)
		})

		healthServerMux.HandleFunc("/liveness", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
		})

		log.Infof("health check \"/readiness\" and \"/liveness\" start on :%d", config.WebApp.HealthPort)
		if err := http.ListenAndServe(fmt.Sprintf(":%d", config.WebApp.HealthPort), healthServerMux); err != nil {
			log.Fatalf("can not start health check server %s", err)
		}

	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 10 seconds.
	gracefulStop := make(chan os.Signal, 1)
	signal.Notify(gracefulStop, os.Interrupt)
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)
	signal.Notify(gracefulStop, syscall.SIGKILL)

	<-gracefulStop

	muReady.Lock()
	ready = false
	muReady.Unlock()
	time.Sleep(time.Duration(config.WebApp.K8SZeroDownTimeThreshold) * time.Second)

	ctx, cancel := context.WithTimeout(context.Background(),
		time.Duration(config.WebApp.GracefulShutdownTimeout)*time.Second)
	defer cancel()

	if err := ws.engine.Shutdown(ctx); err != nil {
		log.Errorf("error shutting down server %s", err)
	} else {
		log.Info("web server gracefully shutdown")

	}

	for contextName, dbConn := range ws.dbConnections {
		if err := dbConn.Close(); err != nil {
			log.Errorf("error web server closing db context name %s %s", contextName, err)
		} else {
			log.Infof("web server database context name: %s gracefully closed", contextName)

		}
	}

	return nil
}

func dbInit(initialScripts []string, contextName string, conn *sql.DB) error {
	if conn == nil {
		return fmt.Errorf("not found database connection at contextname %s", contextName)
	}

	//load sql file
	for _, initialScript := range initialScripts {
		sqlCmd, err := rdsutil.SQLLoader(initialScript)
		if err != nil {
			return err
		}
		if _, err := conn.Exec(sqlCmd); err != nil {
			return err
		}

	}

	return nil
}
