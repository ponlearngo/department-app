package webserver

import (
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/gecthai/enginex/util/fileutil"
	"gitlab.com/gecthai/enginex/util/stringutil"
)

type webSiteValidatorFunc func(site *SiteRegistry) error

var webSiteValidators = []webSiteValidatorFunc{validSiteDuplicationName, validTemplateFile, validPageAPI, validResourcesMapping}

var (
	ErrTemplateWebPageNameRequire = errors.New("web page template name is require")
	ErrTemplateBaseNameRequire    = errors.New("base template name is require")

	ErrTemplateNameDup     = func(name string) error { return fmt.Errorf("template name %s is duplicate", name) }
	ErrTemplateFileEmpty   = errors.New("template file is empty")
	ErrTemplateFileRequire = func(name string) error { return fmt.Errorf("template file in page name %s is require", name) }
	ErrTemplateFileDup     = func(name string, templateFile string) error {
		return fmt.Errorf("template file %s is duplicate in page name %s", templateFile, name)
	}
	ErrTemplateFileNotExist = func(templateFile string) error { return fmt.Errorf("template file %s not exist", templateFile) }
	ErrTemplateFail         = func(name string, templateFile string, err error) error {
		return fmt.Errorf("error generator template %s %s %s", name, templateFile, err)
	}
	ErrTemplateWebPageNotFoundBase = func(baseName string, webPageName string) error {
		return fmt.Errorf("web page name %s not found base tempalte name %s", webPageName, baseName)
	}

	ErrSessionNameReq  = errors.New("session context appender middleware is require session name")
	ErrSessionStoreReq = errors.New("session context appender middleware is require store")

	ErrBasePageAPIURLReq        = func(baseName string) error { return fmt.Errorf("page api in base name %s is require url", baseName) }
	ErrBasePageAPIMethodInvalid = func(baseName string, method string) error {
		return fmt.Errorf("page api in base name %s invalid method %s", baseName, method)
	}

	ErrWebPageURLReq           = func(pageName string) error { return fmt.Errorf("web page name %s is require url", pageName) }
	ErrWebPageAPIMethodInvalid = func(pageName string, method string) error {
		return fmt.Errorf("web page name %s invalid method %s", pageName, method)
	}

	ErrWebPageAPIURLReq            = func(pageName string) error { return fmt.Errorf("web page name %s page api url is not empty", pageName) }
	ErrWebPagePageAPIMethodInvalid = func(pageName string, method string) error {
		return fmt.Errorf("web page name %s page api invalid method %s", pageName, method)
	}

	ErrBaseResourceMappingNotfoundLocalFolder = func(baseName string, folder string) error {
		return fmt.Errorf("error base %s resources mapping source file %s is not exist", baseName, folder)
	}
	ErrBaseResourceMappingCheckLocalFolder = func(baseName string, folder string, err error) error {
		return fmt.Errorf("error base %s resources mapping source file %s %s", baseName, folder, err)
	}
	ErrBaseResourceMappingLocalFolderDup = func(baseName string, folder string) error {
		return fmt.Errorf("error base %s resources mapping source file %s duplicate", baseName, folder)
	}
	ErrBaseResourceMappingTargetDup = func(baseName, target string) error {
		return fmt.Errorf("error base %s resources mapping target file  %s duplicate", baseName, target)
	}
	ErrBaseResourceMappingTargetRequire = func(baseName string) error {
		return fmt.Errorf("error base %s resources mapping target file is not blank", baseName)
	}

	ErrWebPageResourceMappingNotfoundLocalFolder = func(webPageName string, folder string) error {
		return fmt.Errorf("error webpage %s resources mapping source file %s is not exist", webPageName, folder)
	}
	ErrWebPageResourceMappingCheckLocalFolder = func(webPageName string, folder string, err error) error {
		return fmt.Errorf("error webpage %s resources mapping source file %s %s", webPageName, folder, err)
	}
	ErrWebPageResourceMappingLocalFolderDup = func(webPageName string, folder string) error {
		return fmt.Errorf("error webpage %s resources mapping source file %s duplicate", webPageName, folder)
	}
	ErrWebPageResourceMappingTargetDup = func(webPageName, target string) error {
		return fmt.Errorf("error webpage %s resources mapping target file  %s duplicate", webPageName, target)
	}
	ErrWebPageResourceMappingTargetRequire = func(webPageName string) error {
		return fmt.Errorf("error webpage %s resources mapping  target file is not blank", webPageName)
	}
)

var (
	supportHttpMethods = map[string]bool{
		http.MethodGet:     true,
		http.MethodHead:    true,
		http.MethodPost:    true,
		http.MethodPut:     true,
		http.MethodPatch:   true,
		http.MethodDelete:  true,
		http.MethodConnect: true,
		http.MethodOptions: true,
		http.MethodTrace:   true,
	}
)

func validSiteDuplicationName(site *SiteRegistry) error {
	validBaseTemplateNames := make(map[string]bool)
	nameCount := make(map[string]int)
	//count base name and collect base template name
	for _, baseWebPage := range site.BaseWebPages {
		if stringutil.IsEmptyString(baseWebPage.Name) {
			return ErrTemplateBaseNameRequire
		}
		nameCount[baseWebPage.Name]++
		if nameCount[baseWebPage.Name] > 1 {
			return ErrTemplateNameDup(baseWebPage.Name)
		}
		validBaseTemplateNames[baseWebPage.Name] = true
	}

	for _, webPage := range site.WebPages {
		if stringutil.IsEmptyString(webPage.Name) {
			return ErrTemplateWebPageNameRequire
		}
		nameCount[webPage.Name]++
		if nameCount[webPage.Name] > 1 {
			return ErrTemplateNameDup(webPage.Name)
		}
		if stringutil.IsNotEmptyString(webPage.BaseWebPageName) {
			//check base template name
			if _, ok := validBaseTemplateNames[webPage.BaseWebPageName]; !ok {
				return ErrTemplateWebPageNotFoundBase(webPage.BaseWebPageName, webPage.Name)
			}

		}
	}

	return nil
}

func validTemplateFile(site *SiteRegistry) error {
	//validate base page
	for _, baseWebPage := range site.BaseWebPages {
		if len(baseWebPage.TemplateFiles) <= 0 {
			return ErrTemplateFileRequire(baseWebPage.Name)
		}
		templateCount := make(map[string]int)
		for _, templateFile := range baseWebPage.TemplateFiles {
			if stringutil.IsEmptyString(templateFile) {
				return ErrTemplateFileEmpty
			}
			if isExist, _ := fileutil.IsFileExist(templateFile); !isExist {
				return ErrTemplateFileNotExist(templateFile)
			}
			templateCount[templateFile]++
			if templateCount[templateFile] > 1 {
				return ErrTemplateFileDup(baseWebPage.Name, templateFile)
			}
		}
	}

	//validate web page
	for _, webPage := range site.WebPages {
		if len(webPage.TemplateFiles) <= 0 {
			return ErrTemplateFileRequire(webPage.Name)
		}
		templateCount := make(map[string]int)
		for _, templateFile := range webPage.TemplateFiles {
			if stringutil.IsEmptyString(templateFile) {
				return ErrTemplateFileEmpty
			}
			if isExist, _ := fileutil.IsFileExist(templateFile); !isExist {
				return ErrTemplateFileNotExist(templateFile)
			}
			templateCount[templateFile]++
			if templateCount[templateFile] > 1 {
				return ErrTemplateFileDup(webPage.Name, templateFile)
			}
		}
	}

	return nil
}

func validPageAPI(site *SiteRegistry) error {
	for _, baseWebPage := range site.BaseWebPages {
		for _, pageAPI := range baseWebPage.PageAPIs {
			if len(pageAPI.URLs) > 0 {
				for _, url := range pageAPI.URLs {
					if stringutil.IsEmptyString(url) {
						return ErrBasePageAPIURLReq(baseWebPage.Name)
					}
				}
				for _, method := range pageAPI.Methods {
					if !supportHttpMethods[method] {
						return ErrBasePageAPIMethodInvalid(baseWebPage.Name, method)
					}
				}
			}
		}
	}

	for _, webPage := range site.WebPages {
		if len(webPage.URLs) > 0 {
			for _, url := range webPage.URLs {
				if stringutil.IsEmptyString(url) {
					return ErrWebPageURLReq(webPage.Name)
				}
			}
			for _, method := range webPage.Methods {
				if !supportHttpMethods[method] {
					return ErrWebPageAPIMethodInvalid(webPage.Name, method)
				}
			}
		}
		for _, pageAPI := range webPage.PageAPIs {
			if len(pageAPI.URLs) > 0 {
				for _, url := range pageAPI.URLs {
					if stringutil.IsEmptyString(url) {
						return ErrWebPageAPIURLReq(webPage.Name)
					}
				}
				for _, method := range pageAPI.Methods {
					if !supportHttpMethods[method] {
						return ErrWebPagePageAPIMethodInvalid(webPage.Name, method)
					}
				}
			}
		}
	}
	return nil
}

func validResourcesMapping(site *SiteRegistry) error {
	//validate source folder
	targetCount := make(map[string]int)
	sourceCount := make(map[string]int)

	for _, baseWebPage := range site.BaseWebPages {
		for target, source := range baseWebPage.FileResourceMappings {
			targetCount[target]++
			sourceCount[source]++
			isExist, err := fileutil.IsFileExist(source)
			if err != nil {
				return ErrBaseResourceMappingCheckLocalFolder(baseWebPage.Name, source, err)
			}
			if !isExist {
				return ErrBaseResourceMappingNotfoundLocalFolder(baseWebPage.Name, source)
			}
			if stringutil.IsEmptyString(target) {
				return ErrBaseResourceMappingTargetRequire(baseWebPage.Name)
			}

		}

		//valid duplicate
		for target, count := range targetCount {
			if count > 1 {
				return ErrBaseResourceMappingTargetDup(baseWebPage.Name, target)
			}
		}

		for source, count := range sourceCount {
			if count > 1 {
				return ErrBaseResourceMappingLocalFolderDup(baseWebPage.Name, source)
			}
		}

	}

	for _, webPage := range site.WebPages {
		for target, source := range webPage.FileResourceMappings {
			targetCount[target]++
			sourceCount[source]++
			isExist, err := fileutil.IsFileExist(source)
			if err != nil {
				return ErrWebPageResourceMappingCheckLocalFolder(webPage.Name, source, err)
			}
			if !isExist {
				return ErrWebPageResourceMappingNotfoundLocalFolder(webPage.Name, source)
			}
			if stringutil.IsEmptyString(target) {
				return ErrWebPageResourceMappingTargetRequire(webPage.Name)
			}

		}

		//valid duplicate
		for target, count := range targetCount {
			if count > 1 {
				return ErrWebPageResourceMappingTargetDup(webPage.Name, target)
			}
		}

		for source, count := range sourceCount {
			if count > 1 {
				return ErrWebPageResourceMappingLocalFolderDup(webPage.Name, source)
			}
		}

	}

	return nil
}
