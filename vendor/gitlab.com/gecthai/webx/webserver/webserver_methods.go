package webserver

import (
	"github.com/labstack/echo"
	"gitlab.com/gecthai/enginex/database"
	"gitlab.com/gecthai/enginex/redisstore"
	"gitlab.com/gecthai/enginex/session"
)

func (ws *WebServer) Engine() *echo.Echo {
	return ws.engine
}

func (ws *WebServer) DBConnections() database.Connections {
	return ws.dbConnections

}

func (ws *WebServer) SessionStores() session.Stores {
	return ws.sessionStores
}

func (ws *WebServer) CacheStoreConnections() redisstore.CacheStoreConnections {
	return ws.cacheStoreConnections
}
