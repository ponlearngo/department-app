package webserver

import (
	"errors"

	"github.com/labstack/echo"
	"gitlab.com/gecthai/webx/configuration"
)

type WebServerOption func(*WebServer) error

var (
	ErrSiteRegistryRequire        = errors.New("site registry is require")
	ErrPageNotFoundHandlerRequire = errors.New("page not found handler is require")
)

func SiteRegistryOpt(site *SiteRegistry) WebServerOption {
	return func(server *WebServer) error {
		if site == nil {
			return ErrSiteRegistryRequire
		}
		server.website = site

		//prefix additional to base and web page resource
		cfg, err := configuration.Config()
		if err != nil {
			return err
		}
		//mapping template file and file resource
		for idx, _ := range server.website.BaseWebPages {
			for templateIdx, templateFile := range server.website.BaseWebPages[idx].TemplateFiles {
				server.website.BaseWebPages[idx].TemplateFiles[templateIdx] = cfg.WebApp.Resources.ResourcePathPrefix + templateFile
			}
			for target, source := range server.website.BaseWebPages[idx].FileResourceMappings {
				server.website.BaseWebPages[idx].FileResourceMappings[target] = cfg.WebApp.Resources.ResourcePathPrefix + source
			}

		}

		for idx, _ := range server.website.WebPages {
			for templateIdx, templateFile := range server.website.WebPages[idx].TemplateFiles {
				server.website.WebPages[idx].TemplateFiles[templateIdx] = cfg.WebApp.Resources.ResourcePathPrefix + templateFile
			}
			for target, source := range server.website.WebPages[idx].FileResourceMappings {
				server.website.WebPages[idx].FileResourceMappings[target] = cfg.WebApp.Resources.ResourcePathPrefix + source
			}
		}

		return nil
	}
}

func PageNotFoundHandlerOpt(handler echo.HandlerFunc) WebServerOption {
	return func(server *WebServer) error {
		if handler == nil {
			return ErrPageNotFoundHandlerRequire
		}
		server.pageNotFoundHandler = handler
		return nil
	}
}
