package webserver

import (
	"html/template"
	"os"
)

func (ws *WebServer) templateGenerator() (map[string]*template.Template, error) {
	if ws.website == nil {
		return nil, ErrSiteRegistryRequire
	}

	for _, validator := range webSiteValidators {
		err := validator(ws.website)
		if err != nil {
			return nil, err
		}
	}

	//generate base web page
	var baseTemplates map[string]*template.Template = nil

	if len(ws.website.BaseWebPages) >= 0 {
		baseTemplates = make(map[string]*template.Template)
		//generate base template
		for _, baseWebPage := range ws.website.BaseWebPages {
			round := -1
			var baseTemplate *template.Template = nil
			for _, templateFile := range baseWebPage.TemplateFiles {
				round++
				rawContent, err := os.ReadFile(templateFile)
				if err != nil {
					return nil, ErrTemplateFail(baseWebPage.Name, templateFile, err)
				}
				if round == 0 {
					baseTemplate = template.Must(template.New(baseWebPage.Name).Parse(string(rawContent)))
				} else {
					if baseTemplate != nil {
						_, err = baseTemplate.Parse(string(rawContent))
						if err != nil {
							return nil, ErrTemplateFail(baseWebPage.Name, templateFile, err)
						}
					}

				}
			}
			baseTemplates[baseWebPage.Name] = baseTemplate
		}
	}

	//generate web page
	templates := make(map[string]*template.Template)
	for _, webPage := range ws.website.WebPages {
		var webPageTmpl *template.Template = nil
		if baseTemplate, ok := baseTemplates[webPage.BaseWebPageName]; ok {
			webPageTmpl = template.Must(baseTemplate.Clone())
		}
		for _, templateFile := range webPage.TemplateFiles {
			//case require base template
			rawContent, err := os.ReadFile(templateFile)
			if err != nil {
				return nil, ErrTemplateFail(webPage.Name, templateFile, err)
			}
			if webPageTmpl != nil {
				_, err = webPageTmpl.Parse(string(rawContent))
				if err != nil {
					return nil, ErrTemplateFail(webPage.Name, templateFile, err)
				}

			} else {
				webPageTmpl = template.Must(template.New(webPage.Name).Parse(string(rawContent)))
			}

		}

		templates[webPage.Name] = webPageTmpl

	}

	return templates, nil
}
