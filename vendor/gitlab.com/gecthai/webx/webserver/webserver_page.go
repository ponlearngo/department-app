package webserver

import (
	"errors"
	"github.com/labstack/echo"
	"html/template"
	"io"
)

// Define the template registry struct
type TemplateRegistry struct {
	templates map[string]*template.Template
}

// Implement e.Renderer interface
func (t *TemplateRegistry) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	tmpl, ok := t.templates[name]
	if !ok {
		err := errors.New("Template not found " + name)
		return err
	}
	//return tmpl.Execute(w, data)
	return tmpl.Execute(w, data)
}

type SiteRegistry struct {
	BaseWebPages []BaseWebPage
	WebPages     []WebPage
}

type WebPageAPI struct {
	URLs                            []string
	Handler                         echo.HandlerFunc
	Methods                         []string
	ServerAPIMiddleWares            []string
	MiddleWares                     []echo.MiddlewareFunc
	SkipDefaultServerAPIMiddleWares bool
}

type WebPage struct {
	BaseWebPageName string
	//Template Name
	Name string
	//Template Files
	TemplateFiles                   []string
	URLs                            []string
	Methods                         []string
	PageHandler                     echo.HandlerFunc
	MiddleWares                     []echo.MiddlewareFunc
	ServerPageMiddleWares           []string
	SkipDefaultServerAPIMiddleWares bool
	PageAPIs                        []WebPageAPI
	FileResourceMappings            map[string]string
}

type BaseWebPage struct {
	//Template Name
	Name string
	//Template files
	TemplateFiles        []string
	PageAPIs             []WebPageAPI
	FileResourceMappings map[string]string
}
