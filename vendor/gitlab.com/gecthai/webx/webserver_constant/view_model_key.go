package webserver_constant

const(
	CsrfTokenViewModelKey = "csrfToken"
	SignOutWebUrlModelKey = "signOutWebUrl"
	ClientSessionIDViewModelKey = "clientSessionID"
)
