package custom_constant

//service locations
const (
	// TODO User-session
	UserSessionKey      = "user-session"
	CacheUserSessionKey = "cache-user"
	UserSessionId       = "mock-user1234"
	// END

	SeedServiceV1LocationKey    = "seed_service_v1"
	PersonalsService            = "personals"
	PersonalsIndexSearchService = "personal_index"
	PersonalsAddService         = "personal_add"
	PersonalsUpdateService      = "personal_update"

	SessionViewCreditSupportKey = "credit-support-view-code"
	UserDataViewModel           = "userData"
)
