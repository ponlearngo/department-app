
async function init() {
    //initKendoUI();
    //TODO swit fn if is call API
    await initLanguage();
}

async function initLanguage() {
    await i18nLoader(i18nUrl, ["en", "th"]);
    bindDropdownLanguage(currentLang);
    bindI18n(currentLang);
    //document.getElementById("advanced_row_display").style.display = "none"
}

document.addEventListener("DOMContentLoaded", function(event) {
    //initial section
    setTimeout(function() {
        document.body.classList.remove('c-no-layout-transition')
    }, 2000);

    createCookie("current_language", currentLang, 30);
    init();
});


$("#btn_login").click(function(){
    fetch("/seed/v1/credit-support/login", { 
        method: 'POST',
        cache: 'no-cache',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=UTF-8',
            'X-CSRF-Token': csrfToken
        },
        body: JSON.stringify({
            user_name: $("#txt_username").val(),
            password: $("#txt_password").val()
        })
    })
    .then(response => response.json())
    .then(response => {
        if (response['forward_url']) {
            window.location.href = response['forward_url'];
        }
    })
    .catch(err => {
        console.log(err);
    });
});