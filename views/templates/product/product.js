
async function init() {
    //initKendoUI();
    //TODO swit fn if is call API
    iniDataSet();
    await initLanguage();
    selectTab("MULTIPLE")
    bindLogout();
}

async function initLanguage() {
    await i18nLoader(i18nUrl, ["en", "th"]);
    bindDropdownLanguage(currentLang);
    bindI18n(currentLang);
    //document.getElementById("advanced_row_display").style.display = "none"
}
window.dataGridInit = []
window.dataDelete = [];
window.err_msg = []
window.page_action = "NEW"

//TODO remove it
function iniDataSet() {
    fetch("/resources/datasets/product/product_grid.json", {
            method: 'GET',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json'
                    // 'Content-Type': 'application/x-www-form-urlencoded',
            },
        })
        .then(response => response.json())
        .then(responseObj => {
            window.dataGridInit = responseObj;
            initKendoUI();

        })
        .catch(err => {
            console.log(err);
        });
}

async function initKendoUI() {

    //$('#txt_categories_name').kendoTextBox();
    //initial kendoui component ******
    //$('#txt_name').kendoAutoComplete(["kongsak", "saluda"]);
    //$('#txt_lastname').kendoTextBox();

    // $('#txt_sale_price_from').kendoNumericTextBox({
    //     min: 0,
    //     max: 99999999,
    //     step: 1,
    //     value: 0,
    //     format: "n2",
    // });

    // $('#txt_sale_price_to').kendoNumericTextBox({
    //     min: 0,
    //     max: 99999999,
    //     step: 1,
    //     value: 0,
    //     format: "n2",
    // });


    //*** initial data grid
    $("#grd_product_multiple").kendoGrid({
        dataSource: {
            transport: {
                read: function(options) {
                    fetch("/resources/datasets/product/product_grid2.json", {
                            method: 'GET',
                            cache: 'no-cache',
                            headers: {
                                'Content-Type': 'application/json'
                                    // 'Content-Type': 'application/x-www-form-urlencoded',
                            },
                        })
                        .then(response => response.json())
                        .then(responseObj => {
                            console.log(responseObj)
                            options.success(window.dataGridInit);
                            //initKendoUI();

                        })
                        .catch(err => {
                            console.log(err);
                        });

                }
            },
            sort: [{
                field: 'name',
                dir: 'desc'
            }, ],
            schema: {
                data: 'datas',
                total: 'total',
                model: {
                    fields: {
                        "category_code": {
                            type: "string"
                        },
                        "category_name": {
                            type: "string"
                        },
                        "status": {
                            type: "boolean"
                        }
                    }
                },
            },
            serverSorting: true,
            serverFiltering: true,
            serverPaging: false,
            pageSize: 10,
            dataBound: function(e) {
                let grid = getGrid();
                //let items = e.sender.items();
                console.log(grid)
            },
        },
        columns: [{
                field: "product_code",
                title: "Code",
                headerAttributes: {
                    class: "text-left font-weight-bold",
                },
                attributes: {
                    class: "text-left"
                },
                width: "8em",
            },
            {
                field: "product_name",
                title: "Name",
                template: function(data) {
                    let img_c = "";
                    data.product_image.forEach(element => {
                        let tag_img = '<img style="margin-left: 5px;margin-right: 5px;width:35px;height:35px" src="' + element + '"/><label>' + data.product_name + '</label>'
                        img_c += tag_img
                    });

                    return '<div>' + img_c + '</div>'
                },
                headerAttributes: {
                    class: "text-left font-weight-bold",
                },
                attributes: {
                    class: "text-left"
                },
                width: "20em",
            },
            {
                field: "current_stock",
                title: "Current Stock",
                headerAttributes: {
                    class: "text-left font-weight-bold",
                },
                attributes: {
                    class: "text-center"
                },
                width: "6em",
            },
            {
                field: "sale_price",
                title: "Sale Price",
                format: "{0:n2}",
                headerAttributes: {
                    class: "text-left font-weight-bold",
                },
                attributes: {
                    class: "text-center"
                },
                width: "6em",
            },
            {
                field: "update_time",
                title: "Last Update",
                template: "#= kendo.toString(new Date(Number(update_time)), 'dd/MM/yyyy HH:mm:ss') #",
                headerAttributes: {
                    class: "text-center font-weight-bold"
                },
                attributes: {
                    class: "text-center"
                },
                width: "8em",
            },
            {
                title: "Action",
                headerAttributes: {
                    class: "text-center font-weight-bold"
                },
                attributes: {
                    class: "text-center",
                },
                command: [{
                    name: "menu",
                    template: "<div class='btn-edit-context-multiple'><span class='k-icon k-i-more-vertical'> </span></div>",
                    text: "&nbsp;",
                }],

                width: "5em"

            }

        ],
        resizable: true,
        scrollable: true,
        sortable: {
            mode: 'single'
        },
        filterable: false,
        pageable: {
            refresh: true,
            input: true,
            numeric: false,
            responsive: false,
            pageSizes: [10, 25, 50]
        },
    });

    $('#grd_product_multiple .k-grid-content').scroll(function() {
        $('.touch-image').fadeOut(200);
    });

    //mapping event listener
    // document.getElementById("btn_cancel").addEventListener("click", event => {
    //     event.preventDefault();
    //     clearAllErrors();
    // });
    // document.getElementById("btn_save").addEventListener("click", event => {
    //     event.preventDefault();
    //     fetch("../datasets/personal/err_response.json", {
    //         cache: 'no-cache'
    //     })
    //         .then(response => response.json())
    //         .then(response => {
    //             if (!response['success']) {
    //                 bindErrors(response['errors'], currentLang);
    //                 showCurrentErrors(currentLang);
    //             }
    //         })
    //         .catch(error => console.log("", error));
    //     alert("save");
    // });
    document.getElementById("nav-simple-tab").addEventListener("click", event => {
        event.preventDefault();
        $("#uploadImagemodal").modal('show');
    });
   

    $('#addModal').on('show.bs.modal', function(event) {
        let button = $(event.relatedTarget)
        let recipient = button.data('whatever')
        let modal = $(this)
        modal.find('.modal-title').text('New message to ' + recipient)
        modal.find('.modal-body input').val(recipient)
        $('#addCatModal').modal('show');
    })


    // document.getElementById("btn_add").addEventListener("click", event => {
    //     event.preventDefault();
    //     $('.k-invalid-feedback').text('')
    //     let ckLang = getCookie('current_language')
    //     if (ckLang == 'th') {
    //         $('#catModalLabel').text('สร้างหมวดหมู่')
    //     } else {
    //         $('#catModalLabel').text('Create Categories')
    //     }
    //     window.page_action = "NEW"
    //     clearObjectModalData();
    //     $("input[name=ra_status][value='true']").prop("checked", true);
    // });

    document.getElementById("btn_modal_save").addEventListener("click", event => {
        event.preventDefault();
        let json_data = '';
        if ($('#txt_categories_name').val() == '') {
            json_data = "../datasets/categories/err_response.json";
        } else {
            json_data = "../datasets/categories/succcess_response.json"
        }


        if ($('#txt_categories_name').val() != '') {
            fetch(json_data, {
                    cache: 'no-cache'
                })
                .then(response => response.json())
                .then(response => {
                    if (!response['success']) {
                        clearAllErrors();
                        bindErrors(response['errors'], currentLang);
                        showCurrentErrors(currentLang);
                    } else {
                        $('#addCatModal').modal('hide');
                        //TODO remove it
                        alert("Save Success!")
                        if (window.page_action == "NEW") {
                            let combobox = $("#sel_parent_category").data("kendoComboBox").value();
                            let d_flag = $('input[name="ra_status"]:checked').val() == "true" ? true : false;
                            let dataAdd = {
                                category_code: $('#txt_categories_code').val(),
                                category_name: $('#txt_categories_name').val(),
                                parent_category_code: combobox,
                                status: d_flag
                            }
                            $('#grd_product_multiple').data('kendoGrid').dataSource.add(dataAdd)
                        } else {
                            let combobox = $("#sel_parent_category").data("kendoComboBox").value();
                            let d_flag = $('input[name="ra_status"]:checked').val() == "true" ? true : false;

                            let grid = $("#grd_product_multiple").data("kendoGrid");
                            let updatedDataItem = {
                                id: $('#txt_categories_id').val(),
                                category_code: $('#txt_categories_code').val(),
                                category_name: $('#txt_categories_name').val(),
                                parent_category_code: combobox,
                                status: d_flag
                            };

                            let dataItem = grid.dataSource.get(updatedDataItem.id);
                            $.each(updatedDataItem, function(key, val) {
                                if (key === "id") {
                                    return true;
                                }
                                dataItem.set(key, val);
                            });
                        }
                        /*end*/
                    }
                })
                .catch(error => console.log("", error));
        } else {
            fetch("../datasets/categories/err_response.json", {
                    cache: 'no-cache'
                })
                .then(response => response.json())
                .then(response => {
                    if (!response['success']) {
                        bindErrors(response['errors'], currentLang);
                        showCurrentErrors(currentLang);
                    } else {
                        $('#addCatModal').modal('hide');
                        alert("Save Error!")

                    }
                })
                .catch(error => console.log("", error));
        }
    });

    document.getElementById("btn_model_delete_ok").addEventListener("click", event => {
        event.preventDefault();
        deleteData(window.dataDelete.category_code)
        $('#deleteWhModal').modal('hide')

    });


    const content = document.getElementById("content");
    content.style.display = "";
    const footer = document.getElementById("footer");
    footer.style.display = "";
}

function clearTab() {
    /*document.getElementById("nav-simple").innerHTML = "";
    document.getElementById("nav-multiple").innerHTML = "";
    $('#advanced_row_simple').css('display','none');
    $('#advanced_row_multiple').css('display','none');*/
}

window.datas = {
    product_code: "P0001",
    product_name: "เสื้อ",
    barcode: "||i|l|||",
    unit: "50",
    tag_product: "เสื้อ,เสื้อยืด",
    category: "parant 002"
}

function selectTab(id) {
    $("#context-menu").kendoContextMenu({
        target: "#grd_product_multiple",
        filter: ".btn-edit-context-multiple",
        showOn: "click",
        select: function(e) {
            let grid = $("#grd_product_multiple").data("kendoGrid");
            let row = $(e.target).closest("tr");
            let dataItem = grid.dataItem(row);
            let textSelect = $(e.item).children(".k-link").text();
            console.log(dataItem)
            switch (textSelect.trim()) {
                case 'Edit':
                    let ckLang = getCookie('current_language')
                    if (ckLang == 'th') {
                        $('#catModalLabel').text('แก้ไขหมวดหมู่')
                    } else {
                        $('#catModalLabel').text('Edit Categories')
                    }
                    window.page_action = "EDIT"
                    window.page_data = window.datas;
                    console.log(window.page_data.product_code)
                    let type="";
                    if(window.page_data.product_code == "P001"){
                        type = "multiple";
                    }else{
                        type = "single";
                    }
                    fetch("/seed/v1/credit-support/view", { 
                        method: 'POST',
                        cache: 'no-cache',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json; charset=UTF-8',
                            'X-CSRF-Token': csrfToken
                        },
                        body: JSON.stringify({
                            credit_code: window.page_data.product_code,
                            type: type
                        })
                    })
                    .then(response => response.json())
                    .then(response => {
                        if (response['forward_url']) {
                            window.location.href = response['forward_url'];
                        }
                    })
                    .catch(err => {
                        console.log(err);
                    });
                    break;
                case 'Delete':
                    $('#deleteWhModal').modal('show')
                    window.page_action = "DELETE"
                    window.dataDelete = dataItem;
                    break;
                default:
                    break;
            }


        },
        dataSource: [{
                text: "<span class='k-icon k-i-edit'></span><b> Edit</b>",
                encoded: false,
            },
            {
                text: "<span class='k-icon k-i-delete'></span><b> Delete</b>",
                encoded: false,
            }
        ]
    });

    document.getElementById("btn_filtter").addEventListener("click", event => {
        let x = document.getElementById("advanced_row_display");
        if (x.style.display == "none") {
            x.style.display = "";
        } else {
            x.style.display = "none";
        }
    });

    $("#sel_categories").kendoComboBox({
        dataTextField: "category_name",
        dataValueField: "category_code",
        dataSource: {
            transport: {
                read: function(options) {
                    fetch("/resources/datasets/product/sel_categories.json", {
                            method: 'GET',
                            cache: 'no-cache',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                        })
                        .then(response => response.json())
                        .then(responseObj => {
                            options.success(responseObj);
                        })
                        .catch(err => {
                            console.log(err);
                        });
                }
            },
            schema: {
                data: 'datas',
                model: {
                    fields: {
                        "PROVINCE_NAME": {
                            type: "string"
                        },
                        "PROVINCE_ID": {
                            type: "string"
                        },
                    }
                },
            },
        },
    });

    $("#sel_warehouse").kendoComboBox({
        dataTextField: "warehouse_name",
        dataValueField: "warehouse_code",
        dataSource: {
            transport: {
                read: function(options) {
                    fetch("/resources/datasets/product/sel_warehouse.json", {
                            method: 'GET',
                            cache: 'no-cache',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                        })
                        .then(response => response.json())
                        .then(responseObj => {
                            options.success(responseObj);
                        })
                        .catch(err => {
                            console.log(err);
                        });
                }
            },
            schema: {
                data: 'datas',
                model: {
                    fields: {
                        "PROVINCE_NAME": {
                            type: "string"
                        },
                        "PROVINCE_ID": {
                            type: "string"
                        },
                    }
                },
            },
        },
    });

    $("#sel_channel").kendoComboBox({
        dataTextField: "channel_name",
        dataValueField: "channel_code",
        template: function(data) {
            let img = data.channel_image != '' ? data.channel_image : window.image_select_temp
            return '<img style="margin-left: 5px;margin-right: 5px;width: 30px;height:30px" src="' + img + '"/>' +
                '<p style="margin-top: 15px;">' + data.channel_name + '</p>'
        },
        dataSource: {
            transport: {
                read: function(options) {
                    fetch("/resources/datasets/product/sel_channel.json", {
                            method: 'GET',
                            cache: 'no-cache',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                        })
                        .then(response => response.json())
                        .then(responseObj => {
                            options.success(responseObj);
                        })
                        .catch(err => {
                            console.log(err);
                        });
                }
            },
            schema: {
                data: 'datas',
                model: {
                    fields: {
                        "PROVINCE_NAME": {
                            type: "string"
                        },
                        "PROVINCE_ID": {
                            type: "string"
                        },
                    }
                },
            },
        },
    });

    $('#txt_sale_price_from').kendoNumericTextBox({
        min: 0,
        max: 99999999,
        step: 1,
        value: 0,
        format: "n2",
    });

    $('#txt_sale_price_to').kendoNumericTextBox({
        min: 0,
        max: 99999999,
        step: 1,
        value: 0,
        format: "n2",
    });

    // document.getElementById("btn_add").addEventListener("click", event => {
    //     event.preventDefault();
    //     clearAllErrors()
    //     window.page_action = "NEW"
    //     //sessionStorage.setItem('page_action', window.page_action)
    //     window.location.href = '../product/product-simple.html';

    // });

}

function deleteData(code) {
    let dataSource = $("#grd_product_multiple").data("kendoGrid").dataSource;
    let raw = dataSource.data();
    let length = raw.length;
    let item, i;

    for (i = length - 1; i >= 0; i--) {
        item = raw[i];
        if (item.category_code == code) {
            dataSource.remove(item);
        }
    }

    //dataSource.sync();
}

function setObjectModalData(model) {
    if (typeof(model) != "undefined" && model != '') {
        $('#txt_categories_id').val(model.id);
        $('#txt_categories_code').val(model.category_code);
        $('#txt_categories_name').val(model.category_name);
        let combobox = $("#sel_parent_category").data("kendoComboBox");
        combobox.value(model.parent_category_code);
        combobox.trigger("change");
        $("[name=ra_status]").val([model.status]);
    }
}

function clearObjectModalData(model) {
    $('.k-invalid-feedback').text('')
    $('#txt_categories_id').val('')
    $('#txt_categories_code').val('');
    $('#txt_categories_name').val('');
    let combobox = $("#sel_parent_category").data("kendoComboBox");
    combobox.value('');
    combobox.trigger("change");
    $('input[name="ra_status"]:checked').prop('checked', false);
}

$("#sel_parent_category").kendoComboBox({
    dataTextField: "value",
    dataValueField: "id",
    dataSource: {
        transport: {
            read: function(options) {
                fetch("/resources/datasets/categories/cat_drop_down.json", {
                        method: 'GET',
                        cache: 'no-cache',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                    })
                    .then(response => response.json())
                    .then(responseObj => {
                        options.success(responseObj);
                    })
                    .catch(err => {
                        console.log(err);
                    });
            }
        },
        schema: {
            data: 'datas',
            model: {
                fields: {
                    "id": {
                        type: "string"
                    },
                    "value": {
                        type: "string"
                    },
                }
            },
        },
    },
});

document.addEventListener("DOMContentLoaded", function(event) {
    //initial section
    setTimeout(function() {
        document.body.classList.remove('c-no-layout-transition')
    }, 2000);

    createCookie("current_language", currentLang, 30);
    init();
});

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function getBoolean(str) {
    if ("true".startsWith(str)) {
        return true;
    } else if ("false".startsWith(str)) {
        return false;
    } else {
        return null;
    }
}

$('#txt_categories_search').on('input', function(e) {
    let grid = $('#grd_product_multiple').data('kendoGrid');
    let columns = grid.columns;


    let filter = {
        logic: 'or',
        filters: []
    };
    columns.forEach(function(x) {
        if (x.field) {
            let type = grid.dataSource.options.schema.model.fields[x.field].type;
            if (type == 'string') {
                filter.filters.push({
                    field: x.field,
                    operator: 'contains',
                    value: e.target.value
                })
            } else if (type == 'number') {
                if (isNumeric(e.target.value)) {
                    filter.filters.push({
                        field: x.field,
                        operator: 'eq',
                        value: e.target.value
                    });
                }

            } else if (type == 'date') {
                let data = grid.dataSource.data();
                for (let i = 0; i < data.length; i++) {
                    let dateStr = kendo.format(x.format, data[i][x.field]);
                    // change to includes() if you wish to filter that way https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/includes
                    if (dateStr.startsWith(e.target.value)) {
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: data[i][x.field]
                        })
                    }
                }
            } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                let bool = getBoolean(e.target.value);
                filter.filters.push({
                    field: x.field,
                    operator: 'eq',
                    value: bool
                });
            }
        }
    });
    grid.dataSource.filter(filter);
});

// UPLOADFILE
$('#upload_files').on('change', function () {
    let ext = $('#upload_files').val().split(".").pop().toLowerCase();
    let file = this.files[0];
    console.log(file);
    if($.inArray(ext, ["xlsx","pdf",'docx']) == -1) {
        alert("the file is not support'");
        return 0;
    }

    let sizeInKB = (file.size/1024).toFixed(2); 
    let sizeLimit= 10240;

    if (sizeInKB >= sizeLimit) {
        alert("Max file size 10MB");
        return 0;
    }
    fileDocsList.push({
        blob:file,
        type:ext,
        size:file.size,
        file_name:file.name,
    })
    console.log(fileDocsList);
    initPreviewFiles();
    
    $("#upload_files").val('');
});

function onDeleteFile(idx){
    let newArrData = [];
    if(fileDocsList.length>0){
        for(let i = 0 ;i<fileDocsList.length;i++){
            console.log(idx,"-",i);
            if(idx!=i){
                newArrData.push(fileDocsList[i]);
            }
        }
        fileDocsList = newArrData;
        initPreviewFiles();
    }
}

function initPreviewFiles(){
    $("#files-preview-content").html(``)
    if(fileDocsList.length>0){
        for(let i =0 ;i<fileDocsList.length;i++){
            const item = fileDocsList[i]
            $("#files-preview-content").append(`
            <div class="d-flex justify-content-between w-100">
                <div class="show-file-name">${item['file_name']}</div>
                <div class="text-danger k-cursor-pointer" onclick="onDeleteFile(${i})">ลบ</div>
            </div>
            `)
        }
    }
}

function isFileImage(file) {
    const acceptedImageTypes = ['image/jpg', 'image/jpeg', 'image/png'];
    return file && acceptedImageTypes.includes(file['type'])
}

$('#upload_images').on('change', function () {
    let file = this.files[0];
    if(!isFileImage(file)){
       alert("the file is not support'");
       return 0;
    }
    let sizeInKB = (file.size/1024).toFixed(2); 
    let sizeLimit= 10240;
    if (sizeInKB >= sizeLimit) {
        alert("Max file size 10MB");
        return 0;
    }
    fileImagesList.push({
        blob:file,
        type:file['type'],
        size:file.size,
        file_name:file.name,
    })
    console.log(fileImagesList);
    initPreviewImages();
    $("#upload_images").val('');
});

function onDeleteImage(idx){
    let newArrData = [];
    if(fileImagesList.length>0){
        for(let i =0 ;i<fileImagesList.length;i++){
            if(idx!=i){
                newArrData.push(fileImagesList[i]);
            }
        }
        fileImagesList = newArrData;
        initPreviewImages();
    }
}

function initPreviewImages(){
    $("#images-preview-content").html(``)
    if(fileImagesList.length>0){
        for(let i =0 ;i<fileImagesList.length;i++){
            const item = fileImagesList[i]
            $("#images-preview-content").append(`
            <div class="d-flex justify-content-between w-100">
                <div class="show-file-name">${item['file_name']}</div>
                <div class="text-danger k-cursor-pointer" onclick="onDeleteImage(${i})">ลบ</div>
            </div>
            `)
        }
    }
}

$("#btn_upload_files").click(function(){
    const formData = new FormData();
    if(fileImagesList.length>0){
        for(let i =0 ;i<fileImagesList.length;i++){
            const item = fileImagesList[i]
            formData.append('file_image'+i, item.blob, item.file_name)
        }
    }
    if(fileDocsList.length>0){
        for(let i =0 ;i<fileDocsList.length;i++){
            const item = fileDocsList[i]
            formData.append('file_doc'+i, item.blob, item.file_name)
        }
    }
    formObj = {};
    for (var pair of formData.entries()) {
        formObj[pair[0]] = pair[1]
    }
    formData.append('images_length',fileImagesList.length)
    formData.append('files_length',fileDocsList.length)

    if(fileImagesList.length<=0 && fileDocsList.length <= 0){
        Swal.fire({
            title: 'Files is Require!',
            icon: 'error',
            confirmButtonText: 'OK'
        })
        return 0;
    }
    fetch("/seed/v1/upload/files", {
        method: "POST",
            cache: 'no-cache',
            headers: {
                'X-CSRF-Token': csrfToken
            },
            body: formData
        })
        .then(response => response.json())
        .then(response => {
            console.log(response)
            if(!response['success']){
              
            }else{
                Swal.fire({
                    title: 'Upload Success!',
                    icon: 'success',
                    confirmButtonText: 'OK'
                })
                fileImagesList = [];
                fileDocsList = [];
                initPreviewImages();
                initPreviewFiles();
            }
        })
        .catch(error => function() {
            console.log(error);
        });

    console.log(formObj)
    console.log(formData);
})
// 