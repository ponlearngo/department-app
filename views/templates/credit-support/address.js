
async function init() {
    await initLanguage();
    await initData();
}

async function initLanguage() {
    await i18nLoader(i18nUrl, ["en", "th"]);
    bindDropdownLanguage(currentLang);
    bindI18n(currentLang);
}

async function initData(){
    console.log(addressData);
    if(addressData && addressData.datas){
        const itemData = addressData.datas[selectedUserType]
        $("#txt_house_number").val(itemData.house_number);
        $("#txt_house_group").val(itemData.house_group);
        $("#txt_village_name").val(itemData.village_name);
        $("#content-viewer-js").html('<ul id="images-viewer-js" class="images-viewer-js " > </ul>');
        if(itemData.images.length>0){
            for(let i = 0;i<itemData.images.length;i++){
                const image = itemData.images[i]
                $("#images-viewer-js").append(`<li><img src="${image}" class="d-none"></li>`);
            }
            const viewer = new Viewer(document.getElementById('images-viewer-js'), {
                inline: true,
                viewed() {
                  viewer.zoomTo(1);
                },
            });
        }
        
    }
}

function onSelectedProfile(type){
    selectedUserType = type
    $("#tab_borrower").removeClass("active");
    $("#tab_guarantor_1").removeClass("active");
    $("#tab_guarantor_2").removeClass("active");
    $("#tab_guarantor_3").removeClass("active");
    $("#tab_"+selectedUserType).addClass("active");
    initData();
    
}

document.addEventListener("DOMContentLoaded", function(event) {
    //initial section
    setTimeout(function() {
        document.body.classList.remove('c-no-layout-transition')
    
    }, 2000);

    
   
    createCookie("current_language", currentLang, 30);
    init();
});

