
async function init() {
    await initLanguage();
    await initData();
}

async function initLanguage() {
    await i18nLoader(i18nUrl, ["en", "th"]);
    bindDropdownLanguage(currentLang);
    bindI18n(currentLang);
}

async function initData(){
    console.log(creditSupportData);
    if(creditSupportData && creditSupportData.datas){
        let mainTabName = "tab_"+selectedTab;
        if(selectedTab == "borrower"){
            const itemData = creditSupportData.datas[mainTabName]
            const item = itemData[selectedUserType]
            initBorrower(item);

        }
        if(selectedTab == "address"){
            const itemData = creditSupportData.datas[mainTabName]
            const item = itemData[selectedUserType]
            initAddress(item);
        }
      
    }
}

function initBorrower(item){
    $("#txt_first_name").val(item.first_name);
    $("#txt_last_name").val(item.last_name);
    $("#content-viewer-js").html('<ul id="images-viewer-js" class="images-viewer-js " > </ul>');
    if(item.images.length>0){
        for(let i = 0;i<item.images.length;i++){
            const image = item.images[i]
            $("#images-viewer-js").append(`<li><img src="${image}" class="d-none"></li>`);
        }
        const viewer = new Viewer(document.getElementById('images-viewer-js'), {
            inline: true,
            viewed() {
              viewer.zoomTo(1);
            },
        });
    }
}
function initAddress(item){
    $("#txt_house_number").val(item.house_number);
    $("#txt_house_group").val(item.house_group);
    $("#txt_village_name").val(item.village_name);
    $("#content-viewer-js").html('<ul id="images-viewer-js" class="images-viewer-js " > </ul>');
    if(item.images.length>0){
        for(let i = 0;i<item.images.length;i++){
            const image = item.images[i]
            $("#images-viewer-js").append(`<li><img src="${image}" class="d-none"></li>`);
        }
        const viewer = new Viewer(document.getElementById('images-viewer-js'), {
            inline: true,
            viewed() {
              viewer.zoomTo(1);
            },
        });
    }   
}

function disabledSubTab(){
    $("#tab_borrower").addClass("d-none");
    $("#tab_guarantor_1").addClass("d-none");
    $("#tab_guarantor_2").addClass("d-none");
    $("#tab_guarantor_3").addClass("d-none");
}

function enabledSubTab(){
    $("#tab_borrower").removeClass("d-none");
    $("#tab_guarantor_1").removeClass("d-none");
    $("#tab_guarantor_2").removeClass("d-none");
    $("#tab_guarantor_3").removeClass("d-none");
}

function onSelectedProfile(subTabName){
    selectedUserType = subTabName
    $("#tab_borrower").removeClass("active");
    $("#tab_guarantor_1").removeClass("active");
    $("#tab_guarantor_2").removeClass("active");
    $("#tab_guarantor_3").removeClass("active");
    $("#tab_"+selectedUserType).addClass("active");
    initData();
}

function onChangeTab(tabName){
    console.log(tabName);
    $("#main_tab_borrower").removeClass("active");
    $("#main_tab_address").removeClass("active");
    $("#main_tab_mobile").removeClass("active");
    $("#main_tab_goods").removeClass("active");
    $("#main_tab_condition").removeClass("active");
    $("#main_tab_credit_scoring").removeClass("active");
    $("#"+tabName).addClass("active")

    $("#main_tab_borrower_content").addClass("d-none");
    $("#main_tab_address_content").addClass("d-none");
    $("#main_tab_mobile_content").addClass("d-none");
    $("#main_tab_goods_content").addClass("d-none");
    $("#main_tab_condition_content").addClass("d-none");
    $("#main_tab_credit_scoring_content").addClass("d-none");

    if(tabName == "main_tab_borrower"){
        selectedUserType = "borrower";
        selectedTab = "borrower";
        $("#main_tab_borrower_content").removeClass("d-none");
        enabledSubTab()
        onSelectedProfile(selectedUserType);
    }
    if(tabName == "main_tab_address"){
        selectedUserType = "borrower";
        selectedTab = "address";
        $("#main_tab_address_content").removeClass("d-none");
        enabledSubTab()
        onSelectedProfile(selectedUserType);
    }
    if(tabName == "main_tab_mobile"){
        $("#main_tab_mobile_content").removeClass("d-none");
        disabledSubTab();
    }
    if(tabName == "main_tab_goods"){
        $("#main_tab_goods_content").removeClass("d-none");
        disabledSubTab();
    }
    if(tabName == "main_tab_condition"){
        $("#main_tab_condition_content").removeClass("d-none");
        disabledSubTab();
    }
    if(tabName == "main_tab_credit_scoring"){
        $("#main_tab_credit_scoring_content").removeClass("d-none");
        disabledSubTab();
    }
}

document.addEventListener("DOMContentLoaded", function(event) {
    //initial section
    setTimeout(function() {
        document.body.classList.remove('c-no-layout-transition')
    
    }, 2000);

    
   
    createCookie("current_language", currentLang, 30);
    init();
});

