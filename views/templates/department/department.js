
async function init() {
    await initLanguage();
    initKendoUI();
    bindI18n(currentLang);
}

async function initLanguage() {
    await i18nLoader(i18nUrl, ["en", "th"]);
    bindDropdownLanguage(currentLang);
    bindI18n(currentLang);
}

async function initKendoUI() {
    //initial kendoui component ******
    $('#txt_name').kendoAutoComplete(["", ""]);
    $('#txt_lastname').kendoTextBox();
    $('#txt_age').kendoNumericTextBox({
        min: 15,
        max: 200,
        step: 1,
        value: 15,
        format: "n0",
    });
    //*** initial data grid
    $('#grd_department').kendoGrid({
        scrollable: true,
        sortable: { mode: 'single' },
        filterable: false,
        pageable: {
            refresh: true,
            input: true,
            numeric: false,
            pageSizes: [10, 20, 50]
        },
        dataSource: {
            transport: {
                read: function (options) {
                    fetch("/department/v1/departments", {
                        method: 'GET',
                        cache: 'no-cache',
                        headers: {
                            'Content-Type': 'application/json'
                            // 'Content-Type': 'application/x-www-form-urlencoded',
                        },
                    })
                        .then(response => response.json())
                        .then(responseObj => {
                            options.success(responseObj);
                        })
                        .catch(err => {
                            console.log(err);
                        });
                }
            },
            sort: [
                { field: 'department_name', dir: 'asc' },
            ],
            serverSorting: true,
            serverFiltering: true,
            serverPaging: false,
            pageSize: 10,
            schema: {
                data: 'datas',
                total: 'total',
                model: {
                    fields: {
                        "department_name": { type: "string" },
                        "email": { type: "string" },
                        "address": { type: "string" }
                    }
                },
            },
        },
        columns: [
            {
                selectable: true,
                headerAttributes: {
                    class: "text-center",
                },
                attributes: { class: "text-center" },
                width: "3em",
            },
            {
                field: "department_name",
                title: "Department",
                headerAttributes: {
                    class: "text-center font-weight-bold",
                },
                attributes: { class: "text-left" },
                width: "12em",
            },
            {
                field: "email",
                title: "Email",
                headerAttributes: {
                    class: "text-center font-weight-bold"
                },
                attributes: { class: "text-left" },
                width: "16em",
            },
            {
              field: "address",
              title: "Address",
              headerAttributes: {
                  class: "text-center font-weight-bold"
              },
              attributes: { class: "text-left" }
          },
        ],
    });


    const content = document.getElementById("content");
    content.style.display = "";
    const footer = document.getElementById("footer");
    footer.style.display = "";
}

async function AddDepartment() {
  const body = {
    department_name: $("#department-name").val(),
    email: $("#department-email").val(),
    address: $("#department-address").val(),
  };

  $.ajax({
    type: "POST",
    url: '/department/v1/add',
    data:  JSON.stringify(body),
    dataType: "json",
    contentType: "application/json; charset=utf-8",
    success: function (res) {
      GetDataFilter();
      $('#departmentModal').modal('hide')
    },
    error: function($xhr,textStatus,errorThrown) {
      console.log($xhr)
      var err = $xhr.responseJSON;


      // $('#departmentModal').modal('hide')
      setTimeout(() => {
        alert('Error : ' + err.message)
      }, 100);
  }
  });


}

async function GetDataFilter() {

  const body = {
    text_search: $("#txt_search").val(),
    department_name: $("#txt_department").val(),
    email: $("#txt_email").val(),
    address: $("#txt_address").val(),

  };

  $.ajax({
    type: "POST",
    url: '/department/v1/search',
    data:  JSON.stringify(body),
    dataType: "json",
    contentType: "application/json; charset=utf-8",
    success: function (res) {
      var grid = $("#grd_department").data("kendoGrid");
      grid.setDataSource(res.datas);
    },
    error: function(errMsg) {
      console.log(errMsg);
  }
  });


}

async function DeleteDepartment() {
  var grid = $("#grd_department").data("kendoGrid");
  var selectedItem = grid.dataItem(grid.select());

  var rows = grid.select();
  var departmentIds = []
  rows.each(function (index, row) {
    var selectedItem = grid.dataItem(row);
    departmentIds.push(selectedItem?.department_id)
    // selectedItem has EntityVersionId and the rest of your model
  });

  if (departmentIds.length > 0) {
    const body = {
      list: departmentIds
    }
    $.ajax({
      type: "POST",
      url: '/department/v1/delete',
      data:  JSON.stringify(body),
      dataType: "json",
      contentType: "application/json; charset=utf-8",
      success: function (res) {
        GetDataFilter();

      },
      error: function(errMsg) {
        setTimeout(() => {
          alert('Error : ' + errMsg)
        }, 100);
    }
    });
  } 
  else {
    alert('Select Department');
  }

}

document.addEventListener("DOMContentLoaded", function (event) {
    //initial section
    setTimeout(function () {
        document.body.classList.remove('c-no-layout-transition')
    }, 2000);

    createCookie("current_language", currentLang, 30);
    init();


    $("#btn_apply").click(function () {
      GetDataFilter();
      
    });

    
    
  $("#btn-add-person").click(function () {
    AddDepartment();
    
  });


  $("#btn_bulk_delete").click(function () {
    DeleteDepartment();
  });
});
