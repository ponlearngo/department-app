
async function init() {
    await initLanguage();
    initKendoUI();
    bindI18n(currentLang);
}

async function initLanguage() {
    await i18nLoader(i18nUrl, ["en", "th"]);
    bindDropdownLanguage(currentLang);
    bindI18n(currentLang);
}

async function initKendoUI() {
    //initial kendoui component ******
    $('#txt_name').kendoAutoComplete(["", ""]);
    $('#txt_lastname').kendoTextBox();
    $('#txt_age').kendoNumericTextBox({
        min: 15,
        max: 200,
        step: 1,
        value: 15,
        format: "n0",
    });
    //*** initial data grid
    $('#grd_personal').kendoGrid({
        scrollable: true,
        sortable: { mode: 'single' },
        filterable: false,
        pageable: {
            refresh: true,
            input: true,
            numeric: false,
            pageSizes: [10, 20, 50]
        },
        dataSource: {
            transport: {
                read: function (options) {
                    fetch("/resources/datasets/personal/personal_grid.json", {
                        method: 'GET',
                        cache: 'no-cache',
                        headers: {
                            'Content-Type': 'application/json'
                            // 'Content-Type': 'application/x-www-form-urlencoded',
                        },
                    })
                        .then(response => response.json())
                        .then(responseObj => {
                            options.success(responseObj);
                        })
                        .catch(err => {
                            console.log(err);
                        });
                }
            },
            sort: [
                { field: 'name', dir: 'desc' },
            ],
            serverSorting: true,
            serverFiltering: true,
            serverPaging: false,
            pageSize: 10,
            schema: {
                data: 'datas',
                total: 'total',
                model: {
                    fields: {
                        "name": { type: "string" },
                        "last_name": { type: "string" },
                        "age": { type: "number" },
                        "birth_date": { type: "number" }
                    }
                },
            },
        },
        columns: [
            {
                selectable: true,
                headerAttributes: {
                    class: "text-center",
                },
                attributes: { class: "text-center" },
                width: "3em",
            },
            {
                field: "name",
                title: "Name",
                headerAttributes: {
                    class: "text-center font-weight-bold",
                },
                attributes: { class: "text-left" },
                width: "12em",
            },
            {
                field: "last_name",
                title: "Last Name",
                headerAttributes: {
                    class: "text-center font-weight-bold"
                },
                attributes: { class: "text-left" },
                width: "16em",
            },
            {
                field: "age",
                title: "Age",
                headerAttributes: {
                    class: "text-center font-weight-bold"
                },
                attributes: { class: "text-right" },
                width: "5em",
            },
            {
                field: "birth_date",
                title: "Birth Date",
                headerAttributes: {
                    class: "text-center font-weight-bold"
                },
                attributes: { class: "text-center" },
                template: "#= kendo.toString(new Date(Number(birth_date)), 'dd/MM/yyyy') #",
                width: "10em",
            }

        ],
    });


    //mapping event listener
    // document.getElementById("btn_cancel").addEventListener("click", event => {
    //     event.preventDefault();
    //     clearAllErrors();
    // });
    // document.getElementById("btn_save").addEventListener("click", event => {
    //     event.preventDefault();
    //     fetch("resources/datasets/personal/err_response.json", {
    //         cache: 'no-cache'
    //     })
    //         .then(response => response.json())
    //         .then(response => {
    //             if (!response['success']) {
    //                 bindErrors(response['errors'], currentLang);
    //                 showCurrentErrors(currentLang);
    //             }
    //         })
    //         .catch(error => console.log("", error));
    //     alert("save");
    // });

    const content = document.getElementById("content");
    content.style.display = "";
    const footer = document.getElementById("footer");
    footer.style.display = "";
}

document.addEventListener("DOMContentLoaded", function (event) {
    //initial section
    setTimeout(function () {
        document.body.classList.remove('c-no-layout-transition')
    }, 2000);

    createCookie("current_language", currentLang, 30);
    init();
});
