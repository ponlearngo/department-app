package main

import (
	"department-app/handlers"
	"department-app/webapp"
	"log"

	"gitlab.com/gecthai/webx/webserver"
)

func main() {
	serverOpts := []webserver.WebServerOption{
		webserver.SiteRegistryOpt(webapp.Register()),
		webserver.PageNotFoundHandlerOpt(handlers.PageNotFoundHandler),
	}
	server, err := webserver.New(serverOpts...)
	if err != nil {
		log.Fatalf("error start server: %v", err)
	}
	server.Start()

}
