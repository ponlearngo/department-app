package custom_configuration

import (
	"fmt"

	"github.com/spf13/viper"
	"gitlab.com/gecthai/enginex/util/stringutil"
	"gitlab.com/gecthai/webx/configuration"
)

func read() (*CustomConfiguration, error) {
	vp := viper.New()
	vp.AutomaticEnv()

	vp.SetDefault(configuration.EnvKeyCustomConfigFileName, configuration.DefaultCustomConfigFileName)
	configName := vp.GetString(configuration.EnvKeyCustomConfigFileName)
	secretKey := vp.GetString(configuration.EnvKeyAppSecret)
	if stringutil.IsEmptyString(secretKey) {
		return nil, fmt.Errorf("error environment APP_SECRET_KEY is require")
	}

	vp.SetConfigName(configName)
	vp.AddConfigPath("conf")

	if err := vp.ReadInConfig(); err != nil {
		return nil, fmt.Errorf("error reading config file fail, %s", err.Error())
	}

	config := &CustomConfiguration{}
	err := vp.Unmarshal(config)

	if err != nil {
		return nil, fmt.Errorf("error unable to decode into struct, %s", err.Error())
	}

	//validate all configuration
	for _, validFunc := range validConfigValidFuncs {
		if err := validFunc(config); err != nil {
			return nil, err
		}
	}

	return config, nil
}
