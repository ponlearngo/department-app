package custom_configuration

import "encoding/json"

type CustomConfiguration struct {
	DataBaseContext string                          `mapstructure:"database-context" json:"db_context"`
	ServiceLocators map[string]ServiceLocatorConfig `mapstructure:"service-locators" json:"service_locators"`
	// TODO User-session
	CacheStoreName      string `mapstructure:"cache-store-name" json:"cache_store_name"`
	LoginExpireDuration int64  `mapstructure:"login-expire-duration" json:"login_expire_duration"`
	SessionStoreName    string `mapstructure:"session-store-name" json:"session_store_name"`
	IdentityServiceUrl  string `mapstructure:"identity-service-url" json:"identity_service_url"`
	SessionAccountApi   string `mapstructure:"session-account-api" json:"session_account_api"`
	LoginPageURL        string `mapstructure:"login-page-url" json:"login_page_url"`
}

func (cfg CustomConfiguration) String() string {
	jsonRaw, err := json.MarshalIndent(cfg, " ", " ")
	if err != nil {
		return ""
	}
	return string(jsonRaw)
}

type ServiceLocatorConfig struct {
	BaseURL  string            `mapstructure:"base-url" json:"service_locators"`
	Services map[string]string `mapstructure:"services" json:"services"`
}

func (cfg ServiceLocatorConfig) String() string {
	jsonRaw, err := json.MarshalIndent(cfg, " ", " ")
	if err != nil {
		return ""
	}
	return string(jsonRaw)
}
