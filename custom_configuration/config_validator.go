package custom_configuration

import (
	"errors"
	"fmt"

	"github.com/aws/aws-sdk-go/service/accessanalyzer"
	"gitlab.com/gecthai/enginex/util/stringutil"
)

var (
	ErrServiceLocatorRequire = func(locator string) error { return fmt.Errorf("%s in service-locators is require", locator) }
	ErrCacheStoreNameRequire = errors.New("cache-store-name is require")
	ErrServiceBaseURLRequire = func(locator string) error { return fmt.Errorf("base-url in %s require", locator) }
	ErrServiceRequire        = func(locator string, service string) error {
		return fmt.Errorf("%s is require in locator %s", service, accessanalyzer.Locale_Values())
	}
)

var (
	requireLocators = map[string]map[string]bool{
		// custom_constant.SeedServiceV1LocationKey: {
		// 	custom_constant.PersonalsService:            true,
		// 	custom_constant.PersonalsIndexSearchService: true,
		// 	custom_constant.PersonalsAddService:         true,
		// 	custom_constant.PersonalsUpdateService:      true,
		// },
	}
)

func validConfigRequire(cfg *CustomConfiguration) error {
	if stringutil.IsEmptyString(cfg.CacheStoreName) {
		return ErrCacheStoreNameRequire
	}
	for reqLocator, reqServices := range requireLocators {
		serviceLocator, ok := cfg.ServiceLocators[reqLocator]
		if !ok {
			return ErrServiceLocatorRequire(reqLocator)
		}
		for reqService, _ := range reqServices {
			service, ok := serviceLocator.Services[reqService]
			if !ok || stringutil.IsEmptyString(service) {
				return ErrServiceRequire(reqLocator, reqService)
			}
			if stringutil.IsEmptyString(serviceLocator.BaseURL) {
				return ErrServiceBaseURLRequire(reqLocator)
			}
		}

	}
	return nil
}
