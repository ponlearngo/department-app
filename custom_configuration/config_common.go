package custom_configuration

import "fmt"

type ValidCustomConfigurationFunc func(cfg *CustomConfiguration) error

var validConfigValidFuncs = []ValidCustomConfigurationFunc{validConfigRequire}

type CustomAppConfig struct {
	*CustomConfiguration
	serviceLocators map[string]map[string]string
}

var instance *CustomAppConfig = nil

func Config() (*CustomAppConfig, error) {
	if instance == nil {
		config, err := read()
		if err != nil {
			return nil, err
		}
		instance = &CustomAppConfig{
			CustomConfiguration: config,
			serviceLocators:     make(map[string]map[string]string),
		}
	}
	return instance, nil
}

func Reload() (*CustomAppConfig, error) {
	config, err := read()
	if err != nil {
		return nil, err
	}
	instance = &CustomAppConfig{
		CustomConfiguration: config,
		serviceLocators:     make(map[string]map[string]string),
	}
	return instance, nil
}

func (appConf *CustomAppConfig) GetServiceLocation(location, serviceName string) (string, error) {
	if len(appConf.ServiceLocators) > 0 && len(appConf.serviceLocators) <= 0 {
		for locator, services := range appConf.ServiceLocators {
			if len(appConf.serviceLocators[locator]) <= 0 {
				appConf.serviceLocators[locator] = make(map[string]string)
			}
			for serviceName, service := range services.Services {
				appConf.serviceLocators[locator][serviceName] = services.BaseURL + service
			}
		}
	}

	if serviceLocation, ok := appConf.serviceLocators[location][serviceName]; !ok {
		return "", fmt.Errorf("service: %s, name: %s not fond", location, serviceName)
	} else {
		return serviceLocation, nil
	}

}
