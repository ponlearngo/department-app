package webapp

import (
	"department-app/custom_constant"
	"department-app/renderings"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-resty/resty/v2"
	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/gecthai/enginex/common_renderings"
	"gitlab.com/gecthai/enginex/session"
	"gitlab.com/gecthai/webx/webserver_constant"
)

type UserSessionAppenderOptionOutput struct {
	SessionStoreName  string `json:"session_store_name"`
	LoginPageURL      string `json:"login_page_url"`
	AccountRequestUrl string `json:"account_request_url"`
	SkipError         bool   `json:"skip_error"`
}

type UserSessionAppenderOption func() *UserSessionAppenderOptionOutput

func UserSessionAppender(option UserSessionAppenderOption) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(ctx echo.Context) error {
			userSessionAppenderOption := option()
			sessionStoreName := userSessionAppenderOption.SessionStoreName
			loginPageURL := userSessionAppenderOption.LoginPageURL
			accountRequestUrl := userSessionAppenderOption.AccountRequestUrl
			isSkipError := userSessionAppenderOption.SkipError

			getMethod := ctx.Request().Method == http.MethodGet
			//get default view model context
			var vm map[string]interface{}
			if v, ok := ctx.Get(webserver_constant.ViewModelContextKey).(map[string]interface{}); ok {
				vm = v
			} else {
				vm = make(map[string]interface{})
			}
			//get access token
			//read refresh token from session
			errResponse := &common_renderings.Response{
				StatusCode:  http.StatusUnauthorized,
				Success:     false,
				MessageCode: "authe00001",
				Total:       0,
				ForwardURL:  loginPageURL,
			}

			userSession := session.GET(sessionStoreName, ctx)
			if userSession == nil {
				errMessage := fmt.Sprintf("UserSessionAppender --> session store name: %s not found", sessionStoreName)
				log.Warn(errMessage)
				if isSkipError {
					log.Warn("skip to next handler")
					return next(ctx)
				}
				errResponse.Message = errMessage
				if getMethod {
					return ctx.Redirect(http.StatusFound, loginPageURL)
				}
				return ctx.JSON(http.StatusInternalServerError, errResponse)

			}

			iUserId := userSession.Get(custom_constant.UserSessionKey)
			if iUserId == nil {
				errMessage := fmt.Sprintf("UserSessionAppender --> cognito_user_id in session not found")
				log.Warn(errMessage)
				if isSkipError {
					log.Warn("skip to next handler")
					return next(ctx)
				}
				errResponse.Message = errMessage
				if getMethod {
					return ctx.Redirect(http.StatusFound, loginPageURL)
				}
				return ctx.JSON(http.StatusInternalServerError, errResponse)
			}

			userId, matchuserId := iUserId.(string)

			if !matchuserId {
				errMessage := fmt.Sprintf("UserSessionAppender --> cognito_user_id in session not found")
				log.Warn(errMessage)
				if isSkipError {
					log.Warn("skip to next handler")
					return next(ctx)
				}
				errResponse.Message = errMessage
				if getMethod {
					return ctx.Redirect(http.StatusFound, loginPageURL)
				}
				return ctx.JSON(http.StatusInternalServerError, errResponse)
			}

			client := resty.New()
			apiResponse := &common_renderings.Response{}
			request := struct {
				UserId string `json:"user_id"`
			}{
				UserId: userId,
			}
			clientResponse, err := client.R().
				SetHeader("Content-Type", "application/json").
				SetBody(&request).
				SetResult(apiResponse).
				Post(accountRequestUrl)

			if err != nil {
				errMessage := fmt.Sprintf("UserSessionAppender --> %s", err.Error())
				log.Warn(errMessage)
				if isSkipError {
					log.Warn("skip to next handler")
					return next(ctx)
				}
				errResponse.Message = errMessage
				if getMethod {
					return ctx.Redirect(http.StatusFound, loginPageURL)
				}
				return ctx.JSON(http.StatusInternalServerError, errResponse)
			}

			if clientResponse.StatusCode() != http.StatusOK {
				apiResponse := &common_renderings.Response{}
				unmarshalErr := json.Unmarshal(clientResponse.Body(), apiResponse)
				errsResponse := &common_renderings.ErrorsResponse{}

				if unmarshalErr == nil {
					errsResponse.Errors = apiResponse.Errors
					errMessage := fmt.Sprintf("UserSessionAppender --> %s", errsResponse.Error())
					log.Warn(errMessage)
					if isSkipError {
						log.Warn("skip to next handler")
						return next(ctx)
					}
					errResponse.Message = errMessage
					if getMethod {
						return ctx.Redirect(http.StatusFound, loginPageURL)
					}
					return ctx.JSON(http.StatusInternalServerError, errResponse)
				} else {
					errMessage := fmt.Sprintf("UserSessionAppender --> error http status code %d body %s",
						clientResponse.StatusCode(), string(clientResponse.Body()))
					log.Warn(errMessage)
					if isSkipError {
						log.Warn("skip to next handler")
						return next(ctx)
					}
					errResponse.Message = errMessage
					if getMethod {
						return ctx.Redirect(http.StatusFound, loginPageURL)
					}
					return ctx.JSON(http.StatusInternalServerError, errResponse)
				}

			}

			if apiResponse == nil || apiResponse.Datas == nil {
				errMessage := fmt.Sprintf("UserSessionAppender --> response for get user account not found")
				log.Warn(errMessage)
				if isSkipError {
					log.Warn("skip to next handler")
					return next(ctx)
				}
				errResponse.Message = errMessage
				if getMethod {
					return ctx.Redirect(http.StatusFound, loginPageURL)
				}
				return ctx.JSON(http.StatusInternalServerError, errResponse)
			}

			userAccountRaw, match := apiResponse.Datas.(map[string]interface{})
			if !match {
				errMessage := fmt.Sprintf("UserSessionAppender --> response for get user account not found")
				log.Warn(errMessage)
				if isSkipError {
					log.Warn("skip to next handler")
					return next(ctx)
				}
				errResponse.Message = errMessage
				if getMethod {
					return ctx.Redirect(http.StatusFound, loginPageURL)
				}
				return ctx.JSON(http.StatusInternalServerError, errResponse)
			}

			userAccount := &renderings.UserData{}
			cfg := &mapstructure.DecoderConfig{
				Metadata: nil,
				Result:   userAccount,
				TagName:  "json",
			}
			decoder, err := mapstructure.NewDecoder(cfg)
			if err != nil {
				errMessage := fmt.Sprintf("UserSessionAppender --> %s", err.Error())
				log.Warn(errMessage)
				if isSkipError {
					log.Warn("skip to next handler")
					return next(ctx)
				}
				errResponse.Message = errMessage
				if getMethod {
					return ctx.Redirect(http.StatusFound, loginPageURL)
				}
				return ctx.JSON(http.StatusInternalServerError, errResponse)
			}
			err = decoder.Decode(userAccountRaw)
			if err != nil {
				errMessage := fmt.Sprintf("UserSessionAppender --> %s", err.Error())
				log.Warn(errMessage)
				if isSkipError {
					log.Warn("skip to next handler")
					return next(ctx)
				}
				errResponse.Message = errMessage
				if getMethod {
					return ctx.Redirect(http.StatusFound, loginPageURL)
				}
				return ctx.JSON(http.StatusInternalServerError, errResponse)
			}
			vm[custom_constant.UserDataViewModel] = userAccount
			//increment session time
			userSession.Save()
			if len(vm) > 0 {
				ctx.Set(webserver_constant.ViewModelContextKey, vm)
			}
			return next(ctx)
		}
	}
}
