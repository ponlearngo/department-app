package webapp

import (
	"department-app/handlers"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/gecthai/webx/webserver"
)

var productWebPage = webserver.WebPage{
	BaseWebPageName: "base",
	Name:            "productPage",
	TemplateFiles: []string{
		//resource-path-prefix (from configuration) + real path
		productTemplatesPath + "product.gohtml",
		productTemplatesPath + "product-content.gohtml",
	},
	URLs: []string{
		//url path
		"/seed/product-list.html",
	},
	FileResourceMappings: map[string]string{
		//url path : resource-path-prefix (from configuration) + real path
		"/seed/product/productCss.css": productTemplatesPath + "product.css",
		"/seed/product/productJs.js":   productTemplatesPath + "product.js",
	},
	MiddleWares: []echo.MiddlewareFunc{
		UserSessionAppender(userSessionAppenderOpt),
	},
	Methods: []string{
		http.MethodGet,
	},
	PageHandler: handlers.ProductPageHandler,
	PageAPIs: []webserver.WebPageAPI{
		{
			URLs: []string{
				"/seed/v1/credit-support/view",
			},
			Handler: handlers.ViewCreditSupportApiHandler,
			Methods: []string{
				http.MethodPost,
			},
			//SkipDefaultServerAPIMiddleWares: true,
		},
	},
}
