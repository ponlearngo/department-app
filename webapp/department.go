package webapp

import (
	"department-app/handlers"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/gecthai/webx/webserver"
)

var departmentDetailWebPage = webserver.WebPage{
	BaseWebPageName: "base",
	Name:            "departmentDetailPage",
	TemplateFiles: []string{
		//resource-path-prefix (from configuration) + real path
		departmentDetailTemplatesPath + "department.gohtml",
		departmentDetailTemplatesPath + "department-content.html",
	},
	URLs: []string{
		//url path
		"/department/department-detail.html",
	},
	FileResourceMappings: map[string]string{
		//url path : resource-path-prefix (from configuration) + real path
		"/department-detail/department.css": departmentDetailTemplatesPath + "department.css",
		"/department-detail/department.js":  departmentDetailTemplatesPath + "department.js",
	},
	Methods: []string{
		http.MethodGet,
	},
	PageHandler: handlers.DepartmentDetailPageHandler,
	PageAPIs:    []webserver.WebPageAPI{},
}

var departmentWebPage = webserver.WebPage{
	BaseWebPageName: "base",
	Name:            "departmentPage",
	TemplateFiles: []string{
		//resource-path-prefix (from configuration) + real path
		departmentTemplatesPath + "department.gohtml",
		departmentTemplatesPath + "department-content.html",
	},
	URLs: []string{
		//url path
		"/department/department-list.html",
	},
	FileResourceMappings: map[string]string{
		//url path : resource-path-prefix (from configuration) + real path
		"/department/department.css": departmentTemplatesPath + "department.css",
		"/department/department.js":  departmentTemplatesPath + "department.js",
	},
	Methods: []string{
		http.MethodGet,
	},
	PageHandler: handlers.DepartmentPageHandler,
	PageAPIs: []webserver.WebPageAPI{
		{
			URLs: []string{
				"/department/v1/departments",
			},
			Handler: handlers.DepartmentApiHandler,
			Methods: []string{
				http.MethodGet,
			},
			MiddleWares:                     []echo.MiddlewareFunc{},
			SkipDefaultServerAPIMiddleWares: true,
		},
		{
			URLs: []string{
				"/department/v1/add",
			},
			Handler: handlers.AddPersonApiHandler,
			Methods: []string{
				http.MethodPost,
			},
			MiddleWares:                     []echo.MiddlewareFunc{},
			SkipDefaultServerAPIMiddleWares: true,
		},
		{
			URLs: []string{
				"/department/v1/search",
			},
			Handler: handlers.SearchDepartmentApiHandler,
			Methods: []string{
				http.MethodPost,
			},
			MiddleWares:                     []echo.MiddlewareFunc{},
			SkipDefaultServerAPIMiddleWares: true,
		},
		{
			URLs: []string{
				"/department/v1/delete",
			},
			Handler: handlers.DeleteDepartmentApiHandler,
			Methods: []string{
				http.MethodPost,
			},
			MiddleWares:                     []echo.MiddlewareFunc{},
			SkipDefaultServerAPIMiddleWares: true,
		},
	},
}
