package webapp

import (
	"department-app/handlers"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/gecthai/webx/webserver"
)

var seedWebPage = webserver.WebPage{
	BaseWebPageName: "base",
	Name:            "seedPage",
	TemplateFiles: []string{
		//resource-path-prefix (from configuration) + real path
		seedTemplatesPath + "seed.gohtml",
		seedTemplatesPath + "seed-content.gohtml",
	},
	URLs: []string{
		//url path
		"/seed/personal-list.html",
	},
	FileResourceMappings: map[string]string{
		//url path : resource-path-prefix (from configuration) + real path
		"/seed/seed.css": seedTemplatesPath + "seed.css",
		"/seed/seed.js":  seedTemplatesPath + "seed.js",
	},
	Methods: []string{
		http.MethodGet,
	},
	PageHandler: handlers.SeedPageHandler,
	PageAPIs: []webserver.WebPageAPI{{
		URLs: []string{
			"/seed/v1/upload/files",
		},
		Handler: handlers.UploadFilesApiHandler,
		Methods: []string{
			http.MethodPost,
		},
		MiddleWares: []echo.MiddlewareFunc{},
		//SkipDefaultServerAPIMiddleWares: true,
	}},
}
