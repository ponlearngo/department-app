package webapp

import (
	"department-app/handlers"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/gecthai/webx/webserver"
)

var loginWebPage = webserver.WebPage{
	BaseWebPageName: "base",
	Name:            "loginPage",
	TemplateFiles: []string{
		//resource-path-prefix (from configuration) + real path
		loginTemplatesPath + "login.gohtml",
		loginTemplatesPath + "login-content.gohtml",
	},
	URLs: []string{
		//url path
		"/seed/login.html",
	},
	FileResourceMappings: map[string]string{
		//url path : resource-path-prefix (from configuration) + real path
		"/seed/login/login.css": loginTemplatesPath + "login.css",
		"/seed/login/login.js":  loginTemplatesPath + "login.js",
	},
	Methods: []string{
		http.MethodGet,
	},
	PageHandler: handlers.LoginPageHandler,
	PageAPIs: []webserver.WebPageAPI{
		{
			URLs: []string{
				"/seed/v1/credit-support/login",
			},
			Handler: handlers.LoginApiHandler,
			Methods: []string{
				http.MethodPost,
			},
			MiddleWares: []echo.MiddlewareFunc{},
			//SkipDefaultServerAPIMiddleWares: true,
		},
		{
			URLs: []string{
				"/seed/v1/credit-support/logout",
			},
			Handler: handlers.SignoutApiHandler,
			Methods: []string{
				http.MethodPost,
			},
			MiddleWares: []echo.MiddlewareFunc{},
			//SkipDefaultServerAPIMiddleWares: true,
		},
	},
}
