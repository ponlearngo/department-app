package webapp

import "os"

const (
	seedTemplatesPath             = "templates" + string(os.PathSeparator) + "seed" + string(os.PathSeparator)
	productTemplatesPath          = "templates" + string(os.PathSeparator) + "product" + string(os.PathSeparator)
	loginTemplatesPath            = "templates" + string(os.PathSeparator) + "login" + string(os.PathSeparator)
	creditSupportTemplatesPath    = "templates" + string(os.PathSeparator) + "credit-support" + string(os.PathSeparator)
	departmentTemplatesPath       = "templates" + string(os.PathSeparator) + "department" + string(os.PathSeparator)
	departmentDetailTemplatesPath = "templates" + string(os.PathSeparator) + "department-detail" + string(os.PathSeparator)
)
