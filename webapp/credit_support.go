package webapp

import (
	"department-app/handlers"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/gecthai/webx/webserver"
)

var borrowerWebPage = webserver.WebPage{
	BaseWebPageName: "base",
	Name:            "borrowerPage",
	TemplateFiles: []string{
		//resource-path-prefix (from configuration) + real path
		creditSupportTemplatesPath + "borrower.gohtml",
		creditSupportTemplatesPath + "borrower-content.gohtml",
	},
	URLs: []string{
		//url path
		"/seed/credit-support-borrower.html",
	},
	FileResourceMappings: map[string]string{
		//url path : resource-path-prefix (from configuration) + real path
		"/seed/credit-support/borrower.css": creditSupportTemplatesPath + "borrower.css",
		"/seed/credit-support/borrower.js":  creditSupportTemplatesPath + "borrower.js",
	},
	MiddleWares: []echo.MiddlewareFunc{
		UserSessionAppender(userSessionAppenderOpt),
	},
	Methods: []string{
		http.MethodGet,
	},
	PageHandler: handlers.BorrowerPageHandler,
	PageAPIs:    []webserver.WebPageAPI{},
}

var addressWebPage = webserver.WebPage{
	BaseWebPageName: "base",
	Name:            "addressPage",
	TemplateFiles: []string{
		//resource-path-prefix (from configuration) + real path
		creditSupportTemplatesPath + "address.gohtml",
		creditSupportTemplatesPath + "address-content.gohtml",
	},
	URLs: []string{
		//url path
		"/seed/credit-support-address.html",
	},
	FileResourceMappings: map[string]string{
		//url path : resource-path-prefix (from configuration) + real path
		"/seed/credit-support/address.css": creditSupportTemplatesPath + "address.css",
		"/seed/credit-support/address.js":  creditSupportTemplatesPath + "address.js",
	},
	MiddleWares: []echo.MiddlewareFunc{
		UserSessionAppender(userSessionAppenderOpt),
	},
	Methods: []string{
		http.MethodGet,
	},
	PageHandler: handlers.AddressPageHandler,
	PageAPIs:    []webserver.WebPageAPI{},
}

var creditSupportWebPage = webserver.WebPage{
	BaseWebPageName: "base",
	Name:            "creditSupportPage",
	TemplateFiles: []string{
		//resource-path-prefix (from configuration) + real path
		creditSupportTemplatesPath + "credit-support.gohtml",
		creditSupportTemplatesPath + "credit-support-content.gohtml",
	},
	URLs: []string{
		//url path
		"/seed/credit-support-view.html",
	},
	FileResourceMappings: map[string]string{
		//url path : resource-path-prefix (from configuration) + real path
		"/seed/credit-support/credit-support.css": creditSupportTemplatesPath + "credit-support.css",
		"/seed/credit-support/credit-support.js":  creditSupportTemplatesPath + "credit-support.js",
	},
	MiddleWares: []echo.MiddlewareFunc{
		UserSessionAppender(userSessionAppenderOpt),
	},
	Methods: []string{
		http.MethodGet,
	},
	PageHandler: handlers.CreditSupportViewPageHandler,
	PageAPIs:    []webserver.WebPageAPI{},
}
