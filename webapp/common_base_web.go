package webapp

import (
	"os"

	"gitlab.com/gecthai/webx/webserver"
)

const baseTemplatePath = "templates" + string(os.PathSeparator) + "layouts" + string(os.PathSeparator) + "base" + string(os.PathSeparator)

var basePage = webserver.BaseWebPage{
	Name: "base",
	TemplateFiles: []string{
		baseTemplatePath + "header.gohtml",
		baseTemplatePath + "application-bar.gohtml",
		baseTemplatePath + "left-sidebar.gohtml",
		baseTemplatePath + "sidebar.gohtml",
		baseTemplatePath + "footer.gohtml",
		baseTemplatePath + "menu-credit-support.gohtml",
	},
}
