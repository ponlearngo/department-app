package webapp

import (
	"department-app/custom_configuration"
	"fmt"
	"log"

	"gitlab.com/gecthai/webx/webserver"
)

func Register() *webserver.SiteRegistry {
	return &webserver.SiteRegistry{
		BaseWebPages: []webserver.BaseWebPage{
			basePage,
		},
		WebPages: []webserver.WebPage{
			loginWebPage,
			// seedWebPage,
			// productWebPage,
			// borrowerWebPage,
			// addressWebPage,
			// creditSupportWebPage,
			departmentWebPage,
			departmentDetailWebPage,
		},
	}
}

var userSessionAppenderOption *UserSessionAppenderOptionOutput

func userSessionAppenderOpt() *UserSessionAppenderOptionOutput {
	if userSessionAppenderOption == nil {
		conf, err := custom_configuration.Config()
		if err != nil {
			log.Fatal(err)
		}

		getAccountURL := fmt.Sprintf("%s%s", conf.IdentityServiceUrl, conf.SessionAccountApi)

		usrSessionAppenderOpt := &UserSessionAppenderOptionOutput{
			SessionStoreName:  conf.SessionStoreName,
			LoginPageURL:      conf.LoginPageURL,
			AccountRequestUrl: getAccountURL,
		}
		userSessionAppenderOption = usrSessionAppenderOpt
	}
	return userSessionAppenderOption
}
